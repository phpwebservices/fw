<?php
/**
 * Class CSVFile
 *
 * @author Chris Cunningham
 * @date 2015-02-02 22:56:43
 *
 * Generic CSVFile handler this work directly with the file system creating files at $this->filePath location use CSVMemoryFile
 * to create csv files on the fly using memory.
 *
 * This is intended for very large csv files while CSVMemoryFile is for small files.
 */

namespace FW\Models\Files;

class CSVFile
{
    /**
     * @var array
     */
    protected $columns = array(), $data = array();

    /**
     * @var null|resource
     */
    protected $handle = null;

    /**
     * @var string
     */
    protected $filePath = '';

    /**
     * @var int - prefix integer @example 0007 to remove all public read permissions before creating the file.
     */
    protected $permissions = 0007;

    /**
     * @var string - fopen options @example 'r+' pr 'w+'
     *
     * @link http://www.php.net/manual/en/function.fopen.php
     */
    protected $options = 'r+';

    /**
     * @param array $columns
     * @param array $data
     * @param string $filePath
     * @param int - permission bit integer with preceding zeros @example 0007 will remove all public permissions @see umask() php function
     * @param string $options
     * @throws \Exception
     */
    public function __construct(array $columns = array(), array $data = array(), $filePath = '', $permissions = 0007, $options = 'w+')
    {
        if ($filePath) $this->filePath = $filePath;
        if ($permissions) $this->permissions = $permissions;
        if ($options) $this->options = $options;

        if (!$this->setHandle()) {
            throw new \Exception(__CLASS__.'::'.__METHOD__." - Failed to open csv file: {$filePath} - columns:". print_r($columns, true)." data: ".print_r($data, true));
        }

        if (!$this->isColumnsSet() && $columns) {
            $this->addRow($columns);
        }

        if ($data) $this->addRows($data);
    }

    /**
     * Use file system if you know the files are going to be massive.
     *
     * Write csv data to file if desired
     *
     * @throws \Exception
     * @return boolean
     */
    protected function setHandle()
    {
        if ($this->validatePath($this->filePath)) {

            $oldUmask = umask($this->permissions);

            $this->handle = @fopen($this->filePath, $this->options);

            if (!is_resource($this->handle)) {
                umask($oldUmask);
                return false;
            }
            umask($oldUmask);

            return true;
        }
        return false;
    }

    protected function isColumnsSet()
    {
        $stats = fstat($this->handle);

        if (!isset($stats['size']) || !$stats['size']) {
            return false;
        }

        /**
         * Note: If you have opened the file in append (a or a+) mode, any data you
         * write to the file will always be appended, regardless of the file position.
         * so rewind will have no affect on where data is written to the file.
         */
        rewind($this->handle);

        $columns = fgetcsv($this->handle);

        if (!is_array($columns) || !count($columns)) {
            return false;
        }

        return true;
    }

    /**
     * @param array $data
     */
    public function addRows(array $data)
    {
        foreach($data as $row) {
            $this->addRow($row);
        }
    }

    /**
     * @param array $data
     * @throws \Exception
     */
    public function addRow(array $data)
    {
        if (!fputcsv($this->handle, $this->preFormatData($data))) {
            throw new \Exception(__CLASS__."::".__METHOD__." - Failed to write to csv file stream.");
        }
    }

    /**
     * @param $data
     * @return mixed
     */
    private function preFormatData($data)
    {
        foreach($data as &$value) {
            $value = html_entity_decode(urldecode($value), ENT_QUOTES, 'UTF-8');
        }
        return $data;
    }

    public function addBlankRow()
    {
        $this->addRow(array());
    }

    public function addBlankRows($number = 1)
    {
        for ($i = 0; $i < $number; $i++) {
            $this->addRow(array());
        }
    }

    /**
     * !!! Only use this on small csv files, DON'T use on large files.
     *
     * return csv data as a string
     *
     * @return string
     */
    public function getData()
    {
        rewind($this->handle);
        return stream_get_contents($this->handle);
    }

    /**
     * @param $fileName
     */
    public static function outputHeaders($fileName)
    {
        header('Content-Type: application/csv');
        header("Content-Disposition: attachment; filename={$fileName}");
        header('Pragma: no-cache');
    }

    /**
     * @return void
     */
    public function readStream()
    {
        rewind($this->handle);
        fpassthru($this->handle);
        exit();
    }

    /**
     * @param string $filePath
     * @return bool
     * @throws \Exception
     */
    protected function validatePath($filePath)
    {
        $dir = pathinfo($filePath, PATHINFO_DIRNAME);

        if (!file_exists($dir) || !is_dir($dir) || !is_writable($dir)) {
            throw new \Exception(__CLASS__."::".__METHOD__." - Can not write to dir {$filePath}");
        }
        return true;
    }

    public function close()
    {
        return fclose($this->handle);
    }

    public function __destruct()
    {
        if (is_resource($this->handle)) {
            $this->close();
        }
    }

}