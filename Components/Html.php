<?php
/**
 * @author Chris Cunningham
 */

namespace FW\Components;

use FW\Components\AbstractComponent;

class Html extends AbstractComponent
{
    public function output()
    { ?>
        <!DOCTYPE html>
        <html>
        <?php echo $this->component('Head', AbstractComponent::REMOVE_AND_REINDEX); ?>
        <body>
        <?php echo $this->getComponentsOutput(); ?>
        </body>
        </html><?php
    }

}