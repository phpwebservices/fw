<?php
/**
 * @author Chris Cunningham
 */

define('FW_BOOL', 1);
define('FW_STRING', 2);
define('FW_ARRAY', 4);
define('FW_OBJECT', 8);
define('FW_RESOURCE', 16);
define('FW_NULL', 32);
define('FW_NUMERIC', 64);
define('FW_UNKNOWN', 128);