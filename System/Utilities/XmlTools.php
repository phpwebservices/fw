<?php
/**
 * Created by PhpStorm.
 * User: chriscunningham
 * Date: 22/02/2014
 * Time: 00:41
 */

use FW\System\Error;

class XmlTools
{
    /**
     * Names can contain letters, numbers, and other characters
     * Names cannot start with a number or punctuation character
     * Names cannot start with the letters xml (or XML, or Xml, etc)
     * Names cannot contain spaces
     * Allow first char [a-z][A-Z][_]
     * Allowed chars after first char [a-z][A-Z][0-9][-_.]
     *
     * @param string|int $parameterData
     *
     * @return string|int
     */
    public static function filterGetSafeXmlNodeName($parameterData)
    {
        $parameterData = preg_replace('/[^\w\s\d-_\.\"\'\=]/i', '', $parameterData);
        if (is_numeric($parameterData[0])) {
            return "i.{$parameterData}";
        }
        return preg_replace('/^((xml)|([\s\.-]))+/i', "x.", $parameterData);
    }
}