<?php
/**
 * @author Chris Cunningham
 */

namespace FW\System\Root;

abstract class BaseComponent extends BaseClass
{
    /**
     * @return string
     */
    abstract public function output();

    public function hasComponentsCollection()
    {
        return false;
    }

    /**
     * @param string $key
     * @param mixed  $valueData
     */
    protected function set($key, $valueData = null)
    {
        trigger_error("Override set method - Components Data is read only");
    }

    /**
     * @param string $key
     * @param mixed  $valueData
     */
    public function __set($key, $valueData = null)
    {
        trigger_error("Override set method - Components Data is read only");
    }

    /**
     * @return string
     */
    public function __toString()
    {
        ob_start();
        $this->output();
        return ob_get_clean();
    }

}