<?php
/**
 * @author Chris Cunningham
 */

namespace FW\System\Controller;

use FW\System\Root;

class Dispatcher
{
    private static $separator = '-';

    /**
     * @var array $nameSpacePrefix
     */
    private static $nameSpacePrefix = ['App', 'Controllers'];

    /**
     * @param array $nameSpacePrefix
     */
    public static function setNameSpacePrefix(array $nameSpacePrefix)
    {
        self::$nameSpacePrefix = $nameSpacePrefix;
    }

    /**
     * @return array
     */
    public static function getNameSpacePrefixArray()
    {
        return self::$nameSpacePrefix;
    }

    /**
     * @return string - App\Controllers\Main
     */
    public static function getNameSpacePrefixString()
    {
        return implode('\\', self::getNameSpacePrefixArray());
    }

    /**
     * @var array $loadedController
     */
    private static $loadedController = null;

    /**
     * @return array
     */
    public static function getLoadedControllerArray()
    {
        return self::$loadedController;
    }

    /**
     * @return array
     */
    public static function getLoadedControllerContextArray()
    {
        return explode('/', self::getLoadedControllerContextString());
    }

    /**
     * @example if context is http://dev.sitename.com/user/camel-case/test/test/test
     * and only \User\CamelCase.c.php exists then this will return user/camel-case
     * @return bool|string
     */
    public static function getLoadedControllerContextString()
    {
        if (!($foundContext = self::getLoadedControllerArray())) {
            return false;
        }
        foreach ($foundContext as &$contextLevel) {
            $contextLevel = implode(self::$separator, fwUcSplit($contextLevel));
        }
        return strtolower(implode('/', $foundContext));
    }

    /**
     * @param string $contextPath
     *
     * @return mixed
     */
    private static function getControllerPathFromContext($contextPath)
    {
        $contextPath = rawurldecode($contextPath);
        $context = explode('/', $contextPath);
        return self::buildName($context);
    }

    /**
     * @param array $contextArray
     *
     * @return mixed
     */
    private static function &buildName(&$contextArray)
    {
        $separator = preg_quote(self::$separator);
        foreach ($contextArray as &$context) {
            $contextWords = preg_split("/{$separator}/", $context);
            foreach ($contextWords as &$contextWord) {
                $contextWord = ucfirst($contextWord);
            }
            $context = implode('', $contextWords);
        }
        return $contextArray;
    }

    /**
     * @param array $contextArray
     *
     * @return string
     */
    private static function controllerSearchFromContextLevels($contextArray)
    {
        $returnNameSpacePath = '';
        while (count($contextArray)) {
            $nameSpacePath = implode('\\', array_merge(self::getNameSpacePrefixArray(), $contextArray));

            $last = end($contextArray);
            if (\Tools::fileExists(($returnNameSpacePath = "{$nameSpacePath}\\{$last}"), '.c.php')) {
                self::$loadedController = $contextArray;
                return $returnNameSpacePath;
            }

            if (\Tools::fileExists($nameSpacePath, '.c.php')) {
                self::$loadedController = $contextArray;
                return $nameSpacePath;
            }
            array_pop($contextArray);
        }
        return $returnNameSpacePath;
    }

    /**
     * @param $contextPath
     *
     * @throws \Exception
     */
    public static function initiate($contextPath)
    {
        $controllerNameSpace = self::controllerSearchFromContextLevels(
            self::getControllerPathFromContext($contextPath));

        try {
            Collection::i()->add(new $controllerNameSpace)->execute();
        } catch (\Exception $exception) {
            if (!\Settings::i()->useItemSearchContext() || !$exception instanceof \ContextException
                || $exception->getCode() != \ContextException::STATUS_404
            ) {
                throw $exception;
            }
            $controllerNameSpace = \Settings::i()->itemSearchController();

            Collection::i()->add(new $controllerNameSpace)->execute();
        }
    }

}