<?php
/**
 * @author Chris Cunningham
 */

// Global Class

/**
 * Quickly knocked this up... its just for debugging... re write this if i get time but not really important.
 */
class Debug
{
    protected $backTrace = [];

    const PATH_TO_ECLIPSE = "N:\\Win7 Downloads\\Development\\Eclipse\\64 Bit\\Eclipse Juno\\eclipse.exe";

    #const PATH_TO_WORKSPACE = "N:\\Development\\Eclipse Workspace\\CharmeEtTradition-XDebug";
    #const PATH_TO_FRAMEWORK_WORKSPACE = 'N:\\Development\\Eclipse Workspace\\CharmeEtTradition-XDebug';

    #const PATH_TO_WORKSPACE = "N:\\Development\\Eclipse Workspace\\CharmeEtTradition-Dev";
    #const PATH_TO_FRAMEWORK_WORKSPACE = "N:\\Development\\Eclipse Workspace\\CharmeEtTradition-Dev-FW";

    const PATH_TO_WORKSPACE = "N:\\Development\\Eclipse Workspace\\";
    const SITE_PROJECT_FOLDER_NAME = "CharmeEtTradition-Dev | iForIdeas Linux | iForIdeas Live";
    const SITE_FW_PROJECT_FOLDER_NAME = "ZFW-0.1.6 | ZFW-Live";

    //const FILE_PATH = "file:///Users/chriscunningham0/Documents/Development%20Eclipse/PHP/CharmeEtTradition-Dev";
    //const FILE_PATH_WORKSPACE = "file:///Users/chriscunningham0/Documents/Development%20Eclipse/PHP/CharmeEtTradition-Dev-FW";

    protected static $wrap = 'white-space: pre-wrap;white-space: -moz-pre-wrap;white-space: -pre-wrap;white-space: -o-pre-wrap;word-wrap: break-word;';

    const ON = true;

    public static $count = 0;

    public static function pre($dataData = null, $backTrace = null, $expandBox = false)
    {
        self::$count++;

        if (self::$count >= 5) {
            return;
        }

        $t = $backTrace ? $backTrace : debug_backtrace();

        if (PHP_SAPI == 'cli') {
            xdebug_disable();
            print_r(debug_backtrace(null, 2));
            return true;
        }
        ?>
    <pre id="debug_window_<?php echo self::$count; ?>"
         style="<?php echo self::$wrap; ?> overflow: auto; width: 98.2%;position: fixed; bottom: 0; left: 0;color: blue; z-index: 500000; margin: 0; padding: 5px 15px; font-family: Courier New; font-size: 13px; background: #F8F8F8;-moz-box-shadow:inset 0px 0px 8px #000000;-webkit-box-shadow:inset 0px 0px 8px #000000;box-shadow:inset 0px 0px 8px #000000;">
        <span
            style="display: block; padding: 10px 10px 0px 10px; margin: 0px 0px -15px 0px;-webkit-box-shadow: 3px 3px 3px 3px rgba(1, 1, 1, 0.2);box-shadow: 3px 3px 3px 3px rgba(1, 1, 1, 0.2);"><?php

            $t = array_reverse($t);

            $lineNumberCount = 0;
            $messageNameCount = 0;
            $fileNameCharCount = 0;

            $paramsFull = array();

            $documentRoot = $_SERVER['DOCUMENT_ROOT'];

            foreach ($t as $key => &$a) {
                if (!isset($a['line'])) {
                    continue;
                }

                $tempLineNumberCount = strlen($a['line']);
                if ($lineNumberCount < $tempLineNumberCount) {
                    $lineNumberCount = $tempLineNumberCount;
                }

                if (!isset($a['class'])) {
                    $a['class'] = null;
                }

                $tempMessageNameCount = strlen($a['class']) + strlen($a['function']) + 2; // 2 for type count static or instance access

                if ($messageNameCount < $tempMessageNameCount) {
                    $messageNameCount = $tempMessageNameCount;
                }

                $docRoot = explode('/', $documentRoot);

                foreach ($docRoot as $level) {
                    if (!$level) {
                        continue;
                    }
                    $a['file'] = preg_replace("/\/{$level}/", '', $a['file'], 1);
                }

                $a['file'] = str_replace($documentRoot, '', $a['file']);

                $tempFileNameCharCount = strlen($a['file']);
                if ($fileNameCharCount < $tempFileNameCharCount) {
                    $fileNameCharCount = $tempFileNameCharCount;
                }

            }

            print "<b>Document Root: <span style='color: red;'>{$documentRoot}</span></b>" . PHP_EOL . PHP_EOL;

            $count = 1;
            foreach ($t as $key => &$a) {
                if (!isset($a['line'])) {
                    continue;
                }

                $parenthesisArgumentsStr = '';

                $temp1 = $temp2 = array();

                $showArguments = '';

                $paramId = $a['function'] . "_" . str_replace('\\', '_', "{$a['class']}_{$a['line']}");

                if (!empty($a['args'])) {
                    foreach ($a['args'] as $argKey => $paramData) {
                        $linkColour = 'blue';
                        $argType = fwGetType($paramData, true);
                        if (is_object($paramData)) {
                            $argType .= " " . get_class($paramData);
                        }
                        if (is_object($paramData) || is_array($paramData)) {
                            if (is_array($paramData) && !$paramData) {
                                $paramData = 'Empty Array';
                            } else {
                                $paramData = print_r($paramData, true);
                            }
                        }

                        $paramValue = $paramData;
                        $paramsBlock = '';
                        if (strlen($paramValue) > 10) {
                            $paramsBlock = $paramValue;
                            $paramValue = "±";
                            $linkColour = 'LightSeaGreen';
                        }
                        $paramId = self::$count . "{$paramId}_{$argKey}";

                        $paramsBlockTitle = htmlentities($paramsBlock, ENT_QUOTES, "UTF-8");

                        if (strpos($paramsBlock, 'Tools::printTidyHtml') === false) {
                            $paramsBlock = $paramsBlockTitle;
                        }

                        $temp1[] = "<a title='$paramsBlockTitle' style='color: {$linkColour};' href='javascript:void(0);' onclick='showParam(\"{$paramId}_" . self::$count . "\");' style='text-decoration:none'>{$argType} '{$paramValue}'</a>";
                        if (strlen($paramsBlock) > 10) {
                            // For use with php.ini settings
                            // html_errors = On
                            // docref_root = "http://uk3.php.net/manual/en/"
                            $paramsBlock = str_replace(array(
                                '[&lt;a href=&#039;',
                                '&#039;&gt;function.',
                                '&lt;/a&gt;]'
                            ), array('[<a target="_blank" href="', '">function.', '</a>]'), $paramsBlock);

                            if ($count == (count($t) - 1) && $expandBox) {
                                $display = 'block';
                            } else {
                                $display = 'none';
                            }
                            $paramsFull[] = "<div id='{$paramId}_" . self::$count . "' style='float: none; display: {$display}; border: 1px solid #ccc; margin: 15px 15px 0 15px;padding: 10px;background: #F8F8F8;-webkit-box-shadow: 3px 3px 3px 3px rgba(1, 1, 1, 0.2);box-shadow: 3px 3px 3px 3px rgba(1, 1, 1, 0.2);'><b>Param {$argKey}: {$argType}</b><br/><br/>{$paramsBlock}</div>";
                        }

                    }
                    $parenthesisArgumentsStr = implode(', ', $temp1);
                }

                $functionNameCount = $messageNameCount - strlen($a['class']);

                if (!isset($a['type'])) {
                    $a['type'] = null;
                }

                $line = "<b style='color: #13A300;'>" . sprintf("% {$lineNumberCount}s", $a['line']) . "</b>";

                $indent = str_repeat('-', $key + 1);

                $file = sprintf("%-{$fileNameCharCount}s", $a['file']);

                $message = "<b style='color: #007A58'>{$a['class']}</b>";
                $message .= "<b style='color: grey; font-weight: normal;'>";
                $message .= "{$a['type']}</b><b style='color: #B6BD33;'>";
                $message .= ($call = sprintf("%-{$functionNameCount}s",
                        $a['function'] . "({$parenthesisArgumentsStr})") . "</b>");
                $message .= implode(PHP_EOL, $paramsFull);

                $paramsFull = array();

                $click = '';
                if (self::ON) {
                    $pathToOpenFile = addslashes(str_replace('/', '\\', ($a['file'])));
                    $click = "href='javascript:void(0);' onclick='executeFile(\"{$pathToOpenFile}\");'";
                }

                print "<a style='text-decoration: none;' {$click}>{$line} {$indent} <b>{$file}</b></a> - {$message}" . PHP_EOL;
                $count++;
            }

            /* <span style="display: block; background-color: #ECF1EF; border: 1px solid #666666; padding: 5px; margin: 0px; font-size: 14px;"><b style="color: red;"><?php print 'End Data: ';?></b><span style="color: green;"><?php echo stripslashes(var_export($dataData, true));?></span></span></span> */ ?>
			       </span>
			       <a title="<?php echo "{$a['line']}:{$a['file']} - {$a['class']}{$a['type']}{$a['function']}()"; ?>"
                      onmousedown="resizeDebug('debug_window_<?php echo self::$count; ?>');" onmouseup="stopResize();"
                      style="position: absolute; top: 0px; left: 0px; block; text-decoration: none; cursor: n-resize; float: left; height: 11px; width: 100%; -webkit-box-shadow: inset 0px 0px 3px 3px rgba(1, 1, 1, 0.2); box-shadow: inset 0px 0px 3px 3px rgba(1, 1, 1, 0.2);">
                       &nbsp;</a>
        </pre><?php
        if (\Debug::$count) {
            if (\Debug::ON) {
                \Debug::getIDEFileOpener();
            }
            \Debug::getJS();
        }
    }

    public static function prebt($dataData = [])
    {
        self::pre($dataData); ?>

        <pre style="color: blue; margin: 0px;">
        <?php print_r($dataData); ?>
        </pre><?php
    }

    public static function getIDEFileOpener()
    { ?>

        <script type="text/javascript"><!--

            function addslashes(string) {
                return string.replace(/\\/g, '\\\\').
                    replace(/\u0008/g, '\\b').
                    replace(/\t/g, '\\t').
                    replace(/\n/g, '\\n').
                    replace(/\f/g, '\\f').
                    replace(/\r/g, '\\r').
                    replace(/'/g, '\\\'').
                    replace(/"/g, '\\"');
            }

            if (!localStorage.getItem('idePath')) {
                localStorage.setItem('idePath', prompt("Enter the location of you eclipse.exe file", '<?php echo addslashes(self::PATH_TO_ECLIPSE);?>'));
            }

            if (!localStorage.getItem('workspacePath')) {
                localStorage.setItem('workspacePath', prompt("Enter the location of your workspace", '<?php echo addslashes(self::PATH_TO_WORKSPACE);?>'));
            }

            if (!localStorage.getItem('workspaceProjectFolder')) {
                localStorage.setItem('workspaceProjectFolder', prompt("Enter the location of your workspace", '<?php echo addslashes(self::SITE_PROJECT_FOLDER_NAME);?>'));
            }

            if (!localStorage.getItem('workspaceFWProjectFolder')) {
                localStorage.setItem('workspaceFWProjectFolder', prompt("Enter the location of your workspace", '<?php echo addslashes(self::SITE_FW_PROJECT_FOLDER_NAME);?>'));
            }

            function executeFile(sOpenFilePath) {
                netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");

                var sFileToOpen = localStorage.getItem('workspacePath') + localStorage.getItem('workspaceProjectFolder') + sOpenFilePath;

                var Eclipse = Components.classes["@mozilla.org/file/local;1"].createInstance(Components.interfaces.nsILocalFile);
                Eclipse.initWithPath(localStorage.getItem('idePath'));

                var FileToOpen = Components.classes["@mozilla.org/file/local;1"].createInstance(Components.interfaces.nsILocalFile);
                FileToOpen.initWithPath(sFileToOpen);
                if (!FileToOpen.exists()) {
                    sFileToOpen = localStorage.getItem('workspacePath') + localStorage.getItem('workspaceFWProjectFolder') + sOpenFilePath;
                }

                var process = Components.classes["@mozilla.org/process/util;1"].createInstance(Components.interfaces.nsIProcess);
                var args = ["-name Eclipse", "--launcher.openFile", sFileToOpen];

                process.init(Eclipse);
                process.run(false, args, args.length);
            }
            //-->
        </script>
    <?php
    }

    public static function getJS()
    { ?>
        <script type='text/javascript'><!--

            var sClickedElementId = 'debug_window_1';

            var IE = document.all ? true : false;

            if (!IE) document.captureEvents(Event.MOUSEMOVE);

            var tempX = '0';
            var tempY = '0';

            if (!localStorage.getItem('debugHeight')) {
                var aTemp = {};
                aTemp[sClickedElementId] = "400";
                localStorage.setItem('debugHeight', JSON.stringify(aTemp));
            }

            addLoadEvent(updateSizes());

            function addLoadEvent(func) {
                var oldonload = window.onload;
                if (typeof window.onload != 'function') {
                    window.onload = func;
                } else {
                    window.onload = function () {
                        if (oldonload) {
                            oldonload();
                        }
                        func();
                    }
                }
            }
            ;

            function showParam(sParamName) {
                var oStyle = document.getElementById(sParamName);
                if (oStyle.style.display == 'block') {
                    oStyle.style.display = 'none';
                } else {
                    oStyle.style.display = "block";
                }
            }
            ;

            function getMouseXY(e) {
                if (IE) {
                    tempX = event.clientX + document.body.scrollLeft;
                    tempY = event.clientY + document.body.scrollTop;
                } else {
                    tempX = e.pageX;
                    tempY = e.pageY;
                }
                if (tempX < 0) {
                    tempX = 0;
                }
                if (tempY < 0) {
                    tempY = 0;
                }

                var iSize = (window.innerHeight - tempY + window.pageYOffset - 5);
                setSize(iSize);

                updateSize(iSize);

                return true
            }
            ;

            function setSize(iSize) {
                var aHeights = JSON.parse(localStorage.getItem('debugHeight'));
                aHeights[sClickedElementId] = iSize;
                localStorage.setItem('debugHeight', JSON.stringify(aHeights));
            }
            ;

            function updateSize(iSize) {
                document.getElementById(sClickedElementId).style.height = iSize + 'px';
            }
            ;

            function updateSizes() {
                var aHeights = JSON.parse(localStorage.getItem('debugHeight'));
                var iCount = 0;
                for (sKey in aHeights) {
                    if (document.getElementById(sKey)) {
                        document.getElementById(sKey).style.height = aHeights[sKey] + 'px';
                    }
                    //if (document.getElementById('debug_window_'+(iCount+1))) {
                    //   var iCurrentHeight =
                    //	var iPreceedingHeight = document.getElementById('debug_window_'+(iCount+1)).style.height;
                    //}
                    iCount++;
                }
            }
            ;

            function resizeDebug(sId) {
                sClickedElementId = sId;
                document.onmousemove = getMouseXY;
            }
            ;

            function stopResize() {
                document.onmousemove = new function () {
                };
            }
            ;

            //-->
        </script><?php
    }

    public static function getType($valueData)
    {
        switch (1) {
            case is_object($valueData):
                return 'object';
            case is_array($valueData):
                return 'array';
            case is_numeric($valueData):
                return 'number';
            case is_null($valueData):
                return 'null';
            case is_bool($valueData):
                return 'bool';
            case is_resource($valueData):
                return 'resource';
            case is_string($valueData):
                return 'string';
            default:
                return 'unknown';
        }
    }
}function pre($dataData = null, $backTrace = null, $expandBox = false)
{
    if (!\Settings::i()->debugSwitch()) {
        return;
    }
    if ($dataData instanceof ContextException) {
        return;
    }
    \Debug::pre($dataData, $backTrace, $expandBox);
}

function getBackTraceStep($bt, $colour = 'blue')
{
    $class = isset($bt['class']) ? $bt['class'] : null;
    $type = isset($bt['type']) ? $bt['type'] : null;
    $function = isset($bt['function']) ? "{$bt['function']}()" : null;
    $file = isset($bt['file']) ? "{$bt['file']}" : null;
    $line = isset($bt['line']) ? ":{$bt['line']}" : null;

    return "{$file}<b style='color: red;'>{$line}</b> <b style='color: {$colour};'>{$class}{$type}{$function}</b>" . "<br/>";
}

function pree($dataData = null, $backTrace = null)
{
    if (!\Settings::i()->debugSwitch()) {
        return;
    }
    \Debug::pre($dataData, $backTrace);
    exit();
}