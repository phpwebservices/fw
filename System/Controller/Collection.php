<?php
/**
 * @author Chris Cunningham
 */

namespace FW\System\Controller;

use FW\System\Controller\Abstracted\Controller;
use FW\System\Controller\Abstracted\Display;

use FW\System\Root;
use FW\System\Error;
use FW\System\Utilities\Traits\Singleton;

/**
 * @method static \FW\System\Controller\Collection i()
 */
class Collection extends Root\ObjectCollection
{
    /**
     * @var Collection
     * @return Collection
     */
    use Singleton;

    /**
     * @param Controller $controller
     *
     * @return $this
     * @throws Error\Exception
     */
    public function add(Controller $controller)
    {
        if ($this->inCollection($controller) && !$controller->isDispatched()) {
            throw new Error\Exception(get_class($controller) . ": Controller already exists in
                    stack and not executed. infinite loop detected execution halted. StackData: " . print_r($this->data,
                    true));
        }
        parent::addObject($controller);
        return $this;
    }

    /**
     * @param string     $controllerName
     * @param Controller $newController
     *
     * @return \FW\System\Controller\Collection
     */
    public function insertBefore($controllerName, Controller $newController)
    {
        return parent::insertObjectBefore($controllerName, $newController);
    }

    /**
     * @param string     $controllerName
     * @param Controller $newController
     *
     * @return \FW\System\Controller\Collection
     */
    public function insertAfter($controllerName, Controller $newController)
    {
        return parent::insertObjectAfter($controllerName, $newController);
    }

    /**
     * @return void
     */
    public function execute()
    {
        // Use for loop as Controllers can be added whilst looping.
        // foreach's count remains the same as when the loop was first started
        for ($i = 0; $i < $this->count(); $i++) {
            $controller =& $this->iteratorData[$i];
            /* @var $controller \FW\System\Controller\Abstracted\Display */
            if (!$controller->isDispatched()) {
                $controller->executePreciseContextMatch();
                if (!is_null($controller->execute())) {
                    continue;
                }
                $controller->setDispatched();
                if ($controller instanceof Display) {
                    $components = $controller->components();
                    if ($components->isActions()) {
                        $components->actions()->execute();
                    }
                    $controller->outputComponents();
                }
            }
        }
    }

}