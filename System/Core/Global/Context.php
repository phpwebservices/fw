<?php
/**
 * @author Chris Cunningham
 */

use FW\System\Controller\Dispatcher;

/**
 * Global Class
 */
class ContextException extends \FW\System\Error\Exception
{
    /**
     * 404 Not Found
     * @var int
     */
    const STATUS_404 = 404;

    /**
     * 500 Internal Server Error
     * @var int
     */
    const STATUS_500 = 500;

    public function __construct($message, $status = ContextException::STATUS_404)
    {
        parent::__construct($message, $status);
    }
}

/**
 * @method static \Context i()
 */
class Context extends \FW\System\Root\BaseClass
{
    const INDEX_ITEM_NAME = 0;

    /**
     * @var \Context
     * @return \Context
     */
    use \FW\System\Utilities\Traits\Singleton;

    public function __construct()
    {
        $this->setRequestedContext();
        $this->clearGlobalContext();
        $this->data($this->parseContext());
        $this->postParseContext();

        if ($this->getMainContext() != $this->context() && $this->isContextModified()) {
            $this->permanentRedirect($this->stringAbsolute(), \Request::i()->getHttpQueryString());
        }
    }

    /**
     * @var string - The original request_uri
     */
    private $originalContext = '';

    /**
     * @var string $requestedContext
     */
    private $requestedContext = null;

    /**
     * @var array $requestedContextArray
     */
    private $requestedContextArray = [];

    /**
     * @return void
     */
    private function setRequestedContext()
    {
        $this->originalContext = trim(fwUrlEncode(G('c', $_REQUEST)), ' /'); // Don't care about white space or ending/starting forward slash
        $this->requestedContext = \StringTools::filterGetSafeData($this->originalContext);
        $this->requestedContextArray = explode('/', $this->requestedContext);
    }

    /**
     * @return array
     */
    private function parseContext()
    {
        if (!$this->requestedContext) {
            return self::getMainContext();
        }
        $contextArray = explode('/', $this->requestedContext);

        foreach ($contextArray as &$context) {
            $temp = urldecode($context);
            //$context = preg_replace('/(?=[A-Z])/', '-', $temp); // This can do camel case but at a cost means upper case letters in the wrong place will not be corrected
            $context = preg_replace("/([^a-z0-9\-_])+/", '', strtolower($temp));
            $context = trim(preg_replace("/([^a-z0-9])+/", '-', $context), '-');
        }

        return $contextArray;
    }

    public function postParseContext()
    {
        if (in_array($this->first(), ['json', 'xml'])) {
            self::setMainContext(array_merge(self::getMainContext(), array($this->first())));
            $this->data(self::getMainContext());
        }
    }

    /**
     * + Positive iIndex values start from beginning of context count
     * - Minus iIndex values start from end of context count
     *
     * @param integer $index
     *
     * @return string|array
     */
    public function context($index = null)
    {
        if (!is_null($index)) {
            return $this->get($index < 0 ? $this->count() + $index : $index);
        }
        return $this->data();
    }

    /**
     * @return boolean
     */
    public function isContextModified()
    {
        return $this->stringRelative() != $this->originalContext;
    }

    /**
     * @param string $contextData
     * @param null   $defaultData
     *
     * @return array|bool|null|string
     */
    public function last($contextData = '', $defaultData = null)
    {
        return $this->position(-1, $contextData, $defaultData);
    }

    /**
     * @param string $contextData
     * @param null   $defaultData
     *
     * @return string
     */
    public function first($contextData = '', $defaultData = null)
    {
        return $this->position(0, $contextData, $defaultData);
    }

    /**
     * Check if $contextData == context at position $index
     *
     * @param        $index
     * @param string $contextData
     * @param null   $defaultData
     *
     * @return array|bool|null|string
     */
    public function position($index, $contextData = '', $defaultData = null)
    {
        if (is_string($contextData) && $contextData) {
            return $this->context($index) == $contextData;
        }
        if (!$contextData || is_array($contextData) && in_array($this->context($index), $contextData)) {
            return $this->context($index);
        }
        return $defaultData;
    }

    /**
     * @return string
     */
    public function stringRelative()
    {
        return implode('/', $this->context());
    }

    /**
     * @return string
     */
    public function stringAbsolute()
    {
        return \Settings::i()->baseUrl() . '/' . $this->stringRelative();
    }

    /**
     * return string
     */
    public function stringRelativeLoadedController()
    {
        return Dispatcher::getLoadedControllerContextString();
    }


    public function stringAbsoluteLoadedController()
    {
        return \Settings::i()->canonicalBaseUrl() . '/' . Dispatcher::getLoadedControllerContextString();
    }

    /**
     * @return string
     */
    public function baseUrl()
    {
        return \Settings::i()->baseUrl();
    }

    /**
     * @return string
     */
    public function canonicalBaseUrl()
    {
        return \Settings::i()->canonicalBaseUrl();
    }

    /**
     * @param array $parameters
     *
     * @return array
     */
    public static function currentUrl($parameters = [])
    {
        return self::url(\Context::i()->context(), $parameters);
    }

    /**
     * Build an absolute url using the sites base domain name
     *
     * @param array $contextArray
     * @param array $parameters
     *
     * @return array
     */
    public static function url($contextArray = [], $parametersArray = [])
    {
        $separator = '';
        if ($contextArray) {
            $separator = '/';
        }

        $parameters = '';
        if ($parametersArray) {
            $parameters = '?';
            $tempArray = [];
            foreach ($parametersArray as $name => $value) {
                $paramName = rawurlencode(rawurldecode($name));
                $value = rawurlencode(rawurldecode($value)); // Decode first to avoid double encodings
                $tempArray[] = "{$paramName}={$value}";
            }
            $parameters .= implode('&', $tempArray);
        }
        return \Settings::i()->baseUrl() . $separator . implode('/', $contextArray) . $parameters;
    }

    /**
     * @var string $rootDirectory
     */
    private static $rootDirectory = '';

    /**
     * @param string $rootDirectory
     */
    public static function setRootDirectory($rootDirectory)
    {
        self::$rootDirectory = $rootDirectory;
    }

    /**
     * @return string
     */
    private static function getRootDirectory()
    {
        return self::$rootDirectory ? self::$rootDirectory . "/" : '';
    }

    /**
     * @var array $mainContextArray
     */
    private static $mainContextArray = ['main'];

    /**
     * @param array $mainContextArray
     */
    public static function setMainContext(array $mainContextArray)
    {
        self::$mainContextArray = $mainContextArray;
    }

    /**
     * @return array
     */
    public static function getMainContext()
    {
        return self::$mainContextArray;
    }

    /**
     * @return void
     */
    private function clearGlobalContext()
    {
        unset($_REQUEST['c']);
        unset($_POST['c']);
        unset($_GET['c']);
    }

    /**
     * @param null $url
     * @param null $parameters
     */
    public function permanentRedirect($url = null, $parameters = null)
    {
        if ($parameters) {
            $parameters = "/?{$parameters}";
        }
        header("Location: {$url}{$parameters}", true, 301);
    }

}