<?php
/**
 * Created by PhpStorm.
 * User: chriscunningham
 * Date: 13/03/2014
 * Time: 01:05
 */

namespace FW\Models\Locations\PostCodes;

use FW\System\Error\Exception;

class PostCodeMaintainer
{
    const ORIGINAL_SIZE = 154666505;

    protected $sourceUrl = 'http://download.data.ordnancesurvey.co.uk/code-point-open/codepoint.zip';

    protected $fileName = 'postcodes-%s.zip';

    protected $basePath = '/data/';

    protected $sourceFileSize = 0;

    public function __construct($basePath)
    {
        $this->basePath = $basePath . $this->basePath;

        $this->sourceFileSize = \Tools::getSizeOfExternalFile($this->sourceUrl);
    }

    public function setTimeLimitMinuets($timeLimitMinuets)
    {
        set_time_limit(60 * $timeLimitMinuets);
    }

    public function downloadSource()
    {
        if ($this->sourceFileSize < 154666505) {
            throw new Exception('File has decreased in size since creating this code. that can\'t be right?');
        }

        clearstatcache();

        if (file_exists($this->getPath())) {
            $this->log('Already Downloaded', array());
        } else {
            $this->log('Downloaded: ',
                fwExecInBackground("wget -O '{$this->getCmdPath()}' {$this->sourceUrl}", $output));
        }

        return true;
    }

    protected function getPath()
    {
        return $this->basePath . sprintf($this->fileName, date('Ymd'));
    }

    protected function getCmdPath()
    {
        return escapeshellarg($this->getPath());
    }

    public function unzipSource()
    {
        while (($fileSize = $this->getFileSize()) != $this->sourceFileSize) {
            sleep(5);
        }
        $path = escapeshellarg($this->basePath);
        fwExecInBackground("unzip {$this->getCmdPath()} -d {$path}/postcodes", $output);
    }

    private function getFileSize()
    {
        exec("wc -c < {$this->getPath()}", $output);
        return array_pop($output);
    }

    private function log($message, $output)
    {
        fwLog($message . print_r($output, true));
    }

}