<?php
/**
 * Class DataBaseHandler
 */

namespace FW\System\Root;

use FW\System\Utilities\Traits;

/**
 * Class DataBaseHandler
 *
 * @package FW\System\Root
 */
class DataBaseHandler
{
    /**
     * @var DataBaseHandler[]
     */
    protected static $instance = null;

    /**
     * @var \DataBase
     */
    protected $db = null;

    /**
     * @var \PDOStatement
     */
    protected $pdoStatement = null;

    protected $tableName = '';

    protected $id = null; // The auto increment id or insert id

    protected $tableColumnsNames = array(); // should be defined in inheriting class for scope purposes

    protected $onDuplicateKeyUpdateColumns = array();

    private $columnCount = 0;

    const IS_INSERT = true;

    public function __construct($dbInstanceId)
    {
        $this->db = \DataBase::i($dbInstanceId);
    }

    /**
     * @param int $tableName
     * @param int $dbInstanceId
     *
     * @return DataBaseHandler
     */
    public static function i($tableName = 0, $dbInstanceId = \DataBase::MAIN_DB)
    {
        $calledClass = get_called_class();

        if(!isset(static::$instance[$tableName]) || !static::$instance[$tableName] instanceof $calledClass || $tableName) {
            static::$instance[$tableName] = new $calledClass($dbInstanceId);
            if($tableName) {
                static::$instance[$tableName]->setTableName($tableName);
            }
            static::$instance[$tableName]->setTableColumnNames();
        }
        return static::$instance[$tableName];
    }

    /**
     * @return \DataBase
     */
    public function db()
    {
        return $this->db;
    }

    public function setTableName($tableName)
    {
        $this->tableName = $tableName;
    }

    public function getDateTime()
    {
        return date('Y-m-d H:i:s');
    }

    public function getOnDuplicateKeyUpdateColumns()
    {
        return $this->onDuplicateKeyUpdateColumns;
    }

    /**
     * @param mixed $inData
     *
     * @return mixed
     * @throws \Exception
     */
    protected function insert($inData)
    {
        $inData = $this->filterInData($this->prepareParams($inData), static::IS_INSERT);

        $columns = $this->removeAutoIncrementColumn($this->tableColumnsNames);

        $columnNamesArray = array_keys($columns);

        $columnNames = implode('`,`', $columnNamesArray);

        $query = "INSERT INTO {$this->tableName} (`$columnNames`) VALUES ".PHP_EOL;

        $inData = fwSortArrayByArrayKeys($inData, array_flip($columnNamesArray), true);

        $parameters = array();
        foreach($inData as $columnName => $value) {
            $parameters[] = ":{$columnName}";
        }
        $query .= "(".implode(',', $parameters).")";

        $this->pdoStatement = $this->db->prepare($query);

        foreach($inData as $columnName => $value) {
            $this->pdoStatement->bindValue(":$columnName", $value);
        }
        $this->pdoStatement->execute();

        return ($this->id = $this->db->lastInsertId());
    }

    /**
     * @param $inData
     *
     * @return bool
     * @throws \Exception
     */
    protected function insertMulti($inData)
    {
        $inData = $this->filterMultiInData($this->prepareParams($inData), static::IS_INSERT);

        $columns = $this->removeAutoIncrementColumn($this->tableColumnsNames);

        $columnNamesArray = array_keys($columns);

        $columnNames = implode('`,`', $columnNamesArray);

        $sql = "INSERT INTO `{$this->tableName}` (`{$columnNames}`) VALUES ".PHP_EOL;

        $insertRows = array();
        foreach($inData as $bindIndex => &$rowData) {
            $rowData = fwSortArrayByArrayKeys($rowData, array_flip($columnNamesArray), true);
            $insertBinds = array();
            foreach($rowData as $columnName => $columnValue) {
                $insertBinds[] = ":{$columnName}_{$bindIndex}";
            }
            $insertRows[] = "(".implode(',', $insertBinds).")";
        }

        $sql .= implode(','.PHP_EOL, $insertRows);

        if($this->getOnDuplicateKeyUpdateColumns()) {
            $updateValues = array();
            foreach($this->getOnDuplicateKeyUpdateColumns() as $columnName) {
                $updateValues[] = "`{$columnName}` = VALUES(`{$columnName}`)";
            }
            $sql .= " ON DUPLICATE KEY UPDATE ".implode(',', $updateValues);
        }

        $this->pdoStatement = $this->db->prepare($sql);

        reset($inData);

        foreach($inData as $bindIndex => $paramRowData) {
            foreach($paramRowData as $columnName => $columnValue) {
                $this->pdoStatement->bindValue(":{$columnName}_{$bindIndex}", $columnValue);
            }
        }

        $return = $this->pdoStatement->execute();

        return $return;
    }

    /**
     * @param $whereColumn
     * @param $whereColumnValue
     * @param $inData
     * @param bool $fillMissing
     *
     * @return bool
     * @throws \Exception
     */
    protected function update($whereColumn, $whereColumnValue, $inData, $fillMissing = false)
    {
        $inData = $this->filterInData(static::prepareParams($inData), !static::IS_INSERT);

        if(!$inData) return false;

        if(!array_key_exists($whereColumn, $this->tableColumnsNames)) {
            throw new \Exception(__CLASS__."::processUpdate() - Update Column Index not found : $whereColumn");
        }

        if(isset($inData[$whereColumn])) unset($inData[$whereColumn]);

        $columns = $this->removeAutoIncrementColumn($this->tableColumnsNames);

        $columnNamesArray = array_keys($columns);

        $inData = fwSortArrayByArrayKeys($inData, array_flip($columnNamesArray), $fillMissing);

        $query = " UPDATE {$this->tableName} SET ".PHP_EOL;
        $parameters = array();

        foreach($inData as $key => $value) {
            $parameters[$key] = "`$key` = :$key";
        }
        $query .= implode(',', $parameters);
        $query .= " WHERE `$whereColumn` = :$whereColumn";

        $this->pdoStatement = $this->db->prepare($query);

        foreach($inData as $key => $value) {
            $this->pdoStatement->bindValue(":$key", $value);
        }

        $this->pdoStatement->bindValue(":$whereColumn", $whereColumnValue);

        $return = $this->pdoStatement->execute();

        return $return;
    }

    /**
     * @param string $columnName
     * @param mixed $value
     * @param int $type
     *
     * @return array|mixed|\stdClass
     * @throws \Exception
     */
    public function selectOne($columnName, $value, $type = \PDO::FETCH_CLASS)
    {
        $columnName = preg_replace('/[^a-zA-Z0-9_\-]/', '', $columnName); // Sanitize column name data

        $this->pdoStatement = $this->db->prepare("SELECT * FROM `{$this->tableName}` WHERE `{$columnName}` = :$columnName"); /* @var $this->pdoStatement \PDOStatement */
        $this->pdoStatement->bindParam(":$columnName", $value, \PDO::PARAM_STR);
        $this->pdoStatement->execute();

        if(($data = $this->pdoStatement->fetchAll($type))) {
            $data = array_shift($data);
        }

        return $data;
    }

    public function setTableColumnNames()
    {
        if(!$this->tableColumnsNames) {
            $this->pdoStatement = $this->db->prepare("DESCRIBE `{$this->tableName}`");
            $this->pdoStatement->execute();
            foreach($this->pdoStatement->fetchAll(\PDO::FETCH_ASSOC) as $columnInfo) {
                $this->tableColumnsNames[$columnInfo['Field']] = $columnInfo['Extra'];
            }
        }
        $this->columnCount = count($this->tableColumnsNames);
    }

    protected function filterInData(array $inData, $isInsert = self::IS_INSERT)
    {
        $columns = $this->tableColumnsNames;
        if($isInsert) {
            $columns = $this->removeAutoIncrementColumn($columns);
        }
        return array_intersect_key($inData, $columns);
    }

    protected function removeAutoIncrementColumn($columns)
    {
        return array_filter($columns, function($value) {return strpos($value, 'auto_increment') === false;});
    }

    protected function filterMultiInData(array $inData, $isInsert = self::IS_INSERT)
    {
        foreach($inData as &$data) {
            $data = $this->filterInData($data, $isInsert);
        }
        return $inData;
    }

    public function getTableName()
    {
        return $this->tableName;
    }

    protected function prepareParams($data)
    {
        if($data instanceof \stdClass || (isset($data[0]) && $data[0] instanceof \stdClass)) {
            return fwStdClassToArray($data);
        }
        if(!is_array($data)) {
            throw new \Exception(__CLASS__.'::'.__FUNCTION__." - Data passed is not an array: ".var_export($data, true));
        }
        return $data;
    }

    protected function removeUpdateData($data)
    {
        foreach($this->getOnDuplicateKeyUpdateColumns() as $key) {
            if(array_key_exists($key, $data)) {
                unset($data[$key]);
            }
        }
        return $data;
    }

}