<?php
/**
 * @author Chris Cunningham
 */

namespace FW\System\Core;

use FW\System\Root;
use FW\System\Error;

class SessionDataBase extends Error\Exception
{
}

final class SessionDataBaseHandler
{
    /**
     * @var Session
     */
    protected static $instance = null;

    /**
     * @return Session
     */
    public static function i($useDataBaseSession = false)
    {
        if (!self::$instance instanceof SessionDataBaseHandler) {
            self::$instance = new SessionDataBaseHandler($useDataBaseSession);
        }
        return self::$instance;
    }

}