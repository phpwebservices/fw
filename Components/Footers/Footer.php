<?php
/**
 * @author Chris Cunningham
 */

namespace FW\Components\Footers;

use FW\Components\AbstractComponent;

class Footer extends AbstractComponent
{
    public function output()
    { ?>
        <div class="footer">
        <?php echo '&copy; 2011-' . date('Y') . ' ' . \Settings::i()->domain(); ?>
        </div><?php
    }

}