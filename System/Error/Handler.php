<?php
/**
 * @author Chris Cunningham
 */

namespace FW\System\Error;

class Handler
{
    /**
     * @var \Settings
     */
    private $settings = null;

    public static function i(\Settings $settings)
    {
        return new self($settings);
    }

    protected function __construct(\Settings $settings)
    {
        $this->settings = $settings;
        $this->configureHandlers();
    }

    private function configureHandlers()
    {
        ini_set("error_log", $this->getErrorLog());

        error_reporting($this->settings->errorReporting());

        Exception::setDisplayErrorSwitch($this->settings->displayErrorSwitch());
        Exception::setDisplayExceptionErrorSwitch($this->settings->displayExceptionErrorSwitch());
        Exception::setLogErrorSwitch($this->settings->logErrorSwitch());
        Exception::setLogExceptionErrorSwitch($this->settings->logExceptionErrorSwitch());

        if ($this->settings->errorHandlerSwitch()) {
            $this->setHandlers();
        }
    }

    private function getErrorLog()
    {
        if (($fileName = $this->settings->errorLogPath())) {
            $fileNameWithDate = $this->settings->errorLogPath(null, null, true);
            if (!file_exists($fileNameWithDate)) {
                if (!touch($fileNameWithDate)) {
                    @trigger_error(__CLASS__ . '::' . __FUNCTION__ . ' - Fatal Error! Could not create log file.',
                        E_USER_WARNING);
                } else {
                    @$oldMask = umask();
                    @chmod($fileNameWithDate, 0640);
                    @umask($oldMask);

                    return $fileNameWithDate;
                }
            } else {
                return $fileNameWithDate;
            }
            return $fileName;
        }
        return false;
    }

    private function setHandlers()
    {
        set_exception_handler([$this, 'handleException']);
        set_error_handler([$this, 'handleError']);
        if ($this->settings->displayErrorLogsSwitch()) {
            register_shutdown_function([$this, 'debugErrorLogs']);
        }
        register_shutdown_function([$this, 'handleFatalError']);
    }

    public function handleException(\Exception $exception)
    {
        Exception::handleException($exception);
    }

    public function handleError($errorNo, $errorTr, $errorFile, $errorLine)
    {
        if (error_reporting()) { // If suppressed error using @ then don't throw an Exception
            Exception::handleException(new \ErrorException("Handler->handleError() - $errorTr", $errorNo, $errorNo,
                $errorFile, $errorLine));
        }
    }

    public function handleFatalError()
    {
        $errors = error_get_last();
        if ($errors && $errors['type'] == E_ERROR && error_reporting()) { // Only for fatal errors and if suppressed error using @ then don't throw an Exception
            Exception::handleException(new \ErrorException("Handler->handleFatalError() - {$errors['message']}",
                $errors['type'], $errors['type'], $errors['file'], $errors['line']));
        }
    }

    public function debugErrorLogs($lineCount = 5)
    {
        if (!file_exists(($lastLog = $this->settings->errorLogPath()))) {
            Exception::handleException(new \Exception("Handler->debugErrorLogs() - log file not found: {$lastLog}"));
            exit();
        }

        if (\Tools::detectServerOs(\Tools::SERVER_OS_WINDOWS)) {
            $lines = [];
            $pointerPosition = -2;
            $startOfFileReached = false;

            if (!is_resource($logFileHandle = fopen($lastLog, 'r'))) {
                Exception::handleException(new \Exception("Handler->debugErrorLogs() - win log file not found."));
                exit();
            }

            // TODO:: @chrisc separate this functionality out and put it in tools this could be useful it reads lines from file starting at the end of the file
            for ($numberOfLines = $lineCount; $numberOfLines > 0; $numberOfLines--) {
                $lineText = $char = null;
                while ($char !== "\n" && $char !== chr(13)) {
                    $lineText = $char . $lineText;
                    if (fseek($logFileHandle, --$pointerPosition, SEEK_END) == -1) {
                        $startOfFileReached = true;
                        break;
                    }
                    $char = fgetc($logFileHandle);
                }
                if (!$lineText && !$startOfFileReached) {
                    $numberOfLines++;
                    continue;
                }
                $lines[] = $lineText;
            }
            $errors = array_reverse(array_filter($lines));
        } else { // Unix/Linux

            $lastLog = escapeshellarg($lastLog);
            $lastLog = exec("ls -rt {$lastLog}*");

            $lineCount = escapeshellarg($lineCount);
            $error = shell_exec("tail -n $lineCount {$lastLog}");
            $errors = array_filter(explode(PHP_EOL, $error));
        }

        $errors = array_filter($errors, function ($error) {
            preg_match('/^\[([^\]]+)/', $error, $matches);
            foreach ($matches as &$match) {
                $match = preg_replace('/\.[\d]{6}/', '', $match); // Remove micro time if present
            }
            $errorTime = strtotime($matches[1]) + microtime(true) - time();
            return $errorTime >= (time() + microtime() - 5);
        });

        if ($errors) {
            pre(implode(PHP_EOL, $errors), null, true);
        }
    }

}