<?php
/**
 * @author Chris Cunningham
 */

namespace FW\System\Interfaces;

interface View
{
    public function content();

}