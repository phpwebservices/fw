<?php
/**
 * Class Archiver
 *
 * @author Chris Cunningham
 * @date 2015-02-02 22:51:24
 *
 * Objective: Pass a list of DataHandlers to the Archiver using a date, and back up all records that are older than the provided date and write to csv files.
 *
 * Sub objectives:
 * 1. Get records from data base that are older than 24 hours
 * 2. Loop through and add to csv archive file model
 * 3. Only process in batches of $this->max to avoid heavy load on the system.
 * 4. Only delete records if successfully wrote csv files.
 * 5. Report errors for a csv-file/dataHandler/table that failed to archive
 * 6. Allow process to continue with additional DataHandlers while skipping failed archives
 * 7. Delete records once csv data is backed up and no errors
 */

namespace FW\Models\ArchiveTools;

use FW\Models\Files\CSVFile;
use FW\System\Extension\ReflectionClass;

class Archiver {

    /** @var array - List of DataHandlers names to archive */
    private $dataHandlers = array();

    /** @var array - Current table name */
    private $table = array();

    /** @var array - Current table column names */
    private $columnNames = array();

    /** @var string - Datetime used to check for records */
    private $datetime = '';

    /** @var string  */
    private $writeLocation = '';

    /** @var string path where to write the current csv file to */
    private $path = '';

    /** @var string name of the current csv file to write to */
    private $fileName = '';

    /** @var int - Current position of the mysql query limit offset */
    private $pos = 0;

    /** @var int - Max amount of rows to handle at one time */
    private $max = 2000;

    /** @var array - current batch of records */
    private $batch = array();

    /** @var array */
    private $errors = array();

    /** @var \DataBase */
    private $db = null;

    /** @var CSVFile */
    private $csvFile = null;

    /**
     * @var int - Keep track of the created date for a record when it changes switch to new file using format 'Ymd'
     */
    private $dateTracker = 0;

    /**
     * Permissions used by mkdir for the directories created.
     * The settings on my parent directory when writing this were
     *
     * chmod -Rf 770 /var/www/html/www.blank.com/lib/data/archive
     * chmod -Rf g+s /var/www/html/www.blank.com/lib/data/archive
     *
     * @var int
     */
    private $dirPermissions = 0;

    /** @var int - bitwise permissions used by umask and removes public read permissions on the files created @example 0007 */
    private $filePermissions = 0;

    /**
     * @param \DataBase $db
     * @param string $datetime - strtotime style string @example '-24 hours'
     * @param string $writeLocation - The location where your CSV files will be saved to
     * @param int $dirPermissions - The mkdir permission for date directories that will be created. @example $writeLocation.'/2014-01' or $writeLocation.'/2014-02'
     * @param int $filePermissions - The umask permission for csv files that are created. @example 0007 will revoke all public permissions.
     */
    public function __construct($db, $datetime = null, $writeLocation = '', $dirPermissions = 0770, $filePermissions = 0007)
    {
        $this->db = $db;
        $this->datetime = date('Y-m-d H:i:s', strtotime($datetime ?: '-24 hours'));
        $this->writeLocation = $writeLocation;
        $this->dirPermissions = $dirPermissions;
        $this->filePermissions = $filePermissions;
    }

    /**
     * @param $dataHandlers
     */
    public function setDataHandlers($dataHandlers)
    {
        $this->dataHandlers = $dataHandlers;
    }

    /**
     * @return bool
     */
    public function archive()
    {
        foreach ($this->dataHandlers as $dataHandler) {
            try {
                $this->validateDataHandler($dataHandler);
                $this->setTableName($dataHandler);
                $this->setColumnNames($dataHandler);
                $this->preparePath();
                $this->process();
            } catch (\Exception $e) {
                $this->addError($e->getMessage());
            }
        }
        return !$this->getErrors();
    }

    /**
     * @param \FW\System\Root\DataBaseHandler $dataHandler
     * @throws \Exception|\ReflectionException
     */
    private function validateDataHandler($dataHandler)
    {
        $dataHandlerInstance = new ReflectionClass($dataHandler);
        if (!class_exists(($class = get_class($dataHandlerInstance)))) {
            throw new \Exception(__CLASS__."::".__FUNCTION__." - Class does not exist : {$class}");
        }
    }

    /**
     * @param \FW\System\Root\DataBaseHandler $dataHandler
     * @throws \ReflectionException
     */
    private function setTableName($dataHandler)
    {
        $reflectionMethod = new \ReflectionMethod($dataHandler, 'getTableName');
        $this->table = $reflectionMethod->invoke(null, null);
    }

    /**
     * @throws \PDOException
     */
    private function setColumnNames()
    {
        $this->columnNames = array(); // reset
        $dbStatement = $this->db->prepare("DESCRIBE {$this->table}");
        $dbStatement->execute();
        foreach($dbStatement->fetchAll(\PDO::FETCH_ASSOC) as $columnInfo) {
            $this->columnNames[] = $columnInfo['Field'];
        }
    }

    /**
     * @return bool
     * @throws \Exception
     */
    private function process()
    {
        while(($this->batch = $this->getBatch())) {

            foreach ($this->batch as $created => $batch) {

                if ($this->dateTracker != $created) {
                    $this->dateTracker = $created;
                    $this->closeCurrentFile();
                }

                \Tools::createDirectory(($dir = $this->getDirectoryPath()), $this->dirPermissions);

                if (!$this->csvFile instanceof CSVFile) {
                    $this->csvFile = new CSVFile($this->columnNames, $batch, "$dir/{$this->getFileName()}", $this->filePermissions, 'a+');
                } else {
                    $this->csvFile->addRows($batch);
                }
            }
            $this->nextBatch();
        }

        return $this->closeCurrentFile();
    }

    /**
     * @return bool
     * @throws \Exception
     */
    private function closeCurrentFile()
    {
        if (!$this->csvFile instanceof CSVFile) {
            return true; // No records found to archive return false don't throw exception
        }
        if (!$this->csvFile->close()) {
            $this->csvFile = null;
            throw new \Exception(__CLASS__."->".__FUNCTION__." - Something went wrong could not close file halting process for table {$this->table} and moving on.");
        }
        $this->csvFile = null;
        $this->deleteRecords();
        return true;
    }

    /**
     * @return void
     */
    private function nextBatch()
    {
        $this->pos = $this->pos + $this->max;
    }

    /**
     * @return array
     * @throws \PDOException
     */
    private function getBatch()
    {
        $dbStatement = $this->db->prepare("SELECT * FROM `{$this->table}` AS t WHERE t.`created` < :date LIMIT :offset, :rowCount");
        $dbStatement->bindParam(':date', $this->datetime, \PDO::PARAM_STR);
        $dbStatement->bindValue(':offset', $this->pos, \PDO::PARAM_INT);
        $dbStatement->bindParam(':rowCount', $this->max, \PDO::PARAM_INT);
        $dbStatement->execute();

        $results = $dbStatement->fetchAll(\PDO::FETCH_ASSOC);

        $return = array();
        foreach ($results as $result) {
            $return[date('Y-m-d', strtotime($result['created']))][] = $result;
        }
        return $return;
    }

    /**
     * @return bool
     * @throws \Exception|\PDOException
     */
    private function deleteRecords()
    {
        $dbStatement = $this->db->prepare("DELETE FROM `{$this->table}` WHERE `created` < :date");
        $dbStatement->bindValue(':date', $this->dateTracker, \PDO::PARAM_STR);
        if(!$dbStatement->execute()) {
            throw new \Exception(__CLASS__."->".__FUNCTION__." - Failed to delete records from table {$this->table} with a created date older than {$this->dateTracker} 00:00:00");
        }
        return true;
    }

    /**
     * @return void
     */
    private function preparePath()
    {
        $this->path = "{$this->writeLocation}/%s";
        $this->fileName = strtolower(implode('-', array_filter(preg_split('/(?=[A-Z][a-z])/', $this->table))))."-%s.csv";
    }

    /**
     * @return string
     */
    private function getDirectoryPath()
    {
        return sprintf($this->path, substr($this->dateTracker, 0, 7)); // Y-m
    }

    /**
     * @return string
     */
    private function getFileName()
    {
        return sprintf($this->fileName, $this->dateTracker);
    }

    /**
     * @param $message
     * @return void
     */
    private function addError($message)
    {
        $this->errors[] = $message;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

}

