<?php

/**
 * Class CSVMemoryFile
 *
 * @author Chris Cunningham
 * @date 2015-02-02 22:58:59
 *
 * This is recommend for smaller files for creating large csv file Use CSVFile class independently and write directly to the file system.
 *
 * CSVMemoryFile class handler - can be used to work on csv file in memory using streams so that files can
 * be created on the fly with out having to write directly to the file system or using up disk space
 *
 * note: stream php://temp will write to temp files on disk if they are greater the $this->maxMemory
 */

namespace FW\Models\Files;

class CSVMemoryFile extends CSVFile
{
    /**
     * @var int
     */
    protected $maxMemory = 1024; // Default 1Mb, this will switch to file if memory is exceeded

    /**
     * @var int - bitwise int FILE_APPEND|FILE_USE_INCLUDE_PATH|LOCK_EX
     *
     * @link http://www.php.net/manual/en/function.file-put-contents.php
     */
    protected $options = 0;

    /**
     * @param array $columns
     * @param array $data
     * @param string $filePath
     * @param int - permission bit integer with preceding zeros @example 0007
     * @param int|string $options - bitwise int FILE_APPEND|FILE_USE_INCLUDE_PATH|LOCK_EX
     * @throws \Exception
     */
    public function __construct(array $columns = array(), array $data = array(), $filePath = '', $permissions = 0007, $options = null)
    {
        parent::__construct($columns, $data, $filePath, $permissions, $options);
    }

    /**
     * Use context streams so csv files can be created in memory with out using file system. Use for small files only
     */
    protected function setHandle()
    {
        $this->handle = @fopen("php://temp{$this->getMaxMemory()}", 'r+');
        return is_resource($this->handle);
    }

    /**
     * @param int $maxMemory
     */
    public function setMaxMemory($maxMemory)
    {
        $this->maxMemory = $maxMemory;
    }

    /**
     * @return string
     */
    protected function getMaxMemory()
    {
        return $this->maxMemory ? ":maxmemory:{$this->maxMemory}" : '';
    }

    /**
     * Write csv data to file if desired
     *
     * @throws \Exception
     * @return boolean
     */
    public function writeToFile()
    {
        if ($this->validatePath($this->filePath)) {
            $oldUmask = umask($this->permissions);
            if (!file_put_contents($this->filePath, $this->getData(), $this->options)) {
                umask($oldUmask);
                throw new \Exception(__CLASS__.'::'.__METHOD__." - Failed to write csv data to file.");
            }
            umask($oldUmask);
            return true;
        }
        return false;
    }

}