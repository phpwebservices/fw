<?php
/**
 * @author Chris Cunningham
 */

namespace FW\System\Root;

class Collection extends Iterator
{
    public function removeFirst()
    {
        return array_shift($this->iteratorData);
    }

    public function removeLast()
    {
        return array_pop($this->iteratorData);
    }


    /**
     * I tried using unset but it had unexpected behaviour
     * so implemented this for removeCurrent. The problem was that
     * the array needed reindexing once it was unset.
     * @return Ambigous <BaseClass, \FW\Components\AbstractComponent>
     */
    public function removeCurrent()
    {
        $temp = [];
        $returnData = null;
        foreach ($this->iteratorData as $index => $dataData) {
            if ($index == $this->key()) {
                $returnData = $dataData;
                continue;
            }
            $temp[] = $dataData;
        }
        $this->iteratorData = $temp;
        $this->prev();
        return $returnData;
    }

    public function addData($dataData)
    {
        $this->iteratorData[] = $dataData;
        return $this;
    }

    public function data()
    {
        return $this->iteratorData;
    }

    public function replace($collection)
    {
        $this->iteratorData = [];
        $this->iteratorData[] = $collection;
        return $this;
    }

}