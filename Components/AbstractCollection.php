<?php
/**
 * @author Chris Cunningham
 */

namespace FW\Components;

use FW\System\Root;
use FW\System\Error;
use FW\Components\Content;

class AbstractCollection extends Root\ObjectCollection
{
    /**
     * @param Root\BaseComponent $component
     *
     * @return \FW\Components\AbstractCollection
     */
    public function add(Root\BaseComponent $component)
    {
        parent::addObject($component);
        return $this;
    }

    /**
     * @param string $componentClassName
     * @param bool   $removeAndReIndex - optional when true will return component and remove from the stack
     *
     * @return bool|AbstractComponent|Root\BaseClass
     * @throws \FW\System\Error\Exception
     */
    public function component($componentClassName, $removeAndReIndex = false)
    {
        foreach ($this as $abstractComponent) {
            /* @var $abstractComponent AbstractComponent */
            if ($abstractComponent->hasComponentsCollection()) {
                $collection = $abstractComponent->getComponentsCollection();
                if (($component = $collection->component($componentClassName,
                        $removeAndReIndex)) instanceof AbstractComponent
                ) {
                    /* @var $component AbstractComponent */
                    return $component;
                }
            }
            return $this->getObject($componentClassName, $removeAndReIndex);
        }
        trigger_error(get_class($this) . '->' . __FUNCTION__ . "(\$componentClassName {$componentClassName}, \$removeAndReIndex " . ($removeAndReIndex ? 'true' : 'false') . ") - Component not found.");
    }

    /**
     * @param string            $objectNameSpaceName
     * @param AbstractComponent $component
     *
     * @return \FW\Components\AbstractCollection
     */
    public function insertBefore($objectNameSpaceName, AbstractComponent $component)
    {
        return $this->insertObjectBefore($objectNameSpaceName, $component);
    }

    /**
     * @param string            $objectNameSpaceName
     * @param AbstractComponent $component
     *
     * @return \FW\Components\AbstractCollection
     */
    public function insertAfter($objectNameSpaceName, AbstractComponent $component)
    {
        return $this->insertObjectAfter($objectNameSpaceName, $component);
    }

    /**
     * @param AbstractComponent $insertComponent
     * @param string            $insertIntoComponentNameSpace
     *
     * @return AbstractComponent
     */
    public function insertInto(AbstractComponent $insertComponent, $insertIntoComponentNameSpace)
    {
        foreach ($this as $abstractComponent) {
            /* @var $abstractComponent AbstractComponent */
            if ($abstractComponent instanceof $insertIntoComponentNameSpace) {
                return $abstractComponent->add($insertComponent);
            }
            if ($abstractComponent->hasComponentsCollection()) {
                $collection = $abstractComponent->getComponentsCollection();
                if (($insertIntoComponent = $collection->insertInto($insertComponent,
                        $insertIntoComponentNameSpace)) instanceof AbstractCollection
                ) {
                    return $insertIntoComponent;
                }
            }
        }
    }

    /**
     * @return boolean
     */
    public function isActions()
    {
        return method_exists($this, 'actions');
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function getOutput(array &$data = [])
    {
        foreach ($this as $abstractComponent) {
            /* @var $abstractComponent AbstractComponent */
            $abstractComponent->setAggregateData($data);
        }
        return implode(PHP_EOL, $this->data());
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function printOutput(array &$data = [])
    {
        print $this->getOutput($data);
    }

}