<?php
/**
 * @author Chris Cunningham
 */

namespace FW\System\Interfaces;

interface DataBean
{
    /**
     * @param array $data
     *
     * @return void
     */
    public function data(array $data = []);

    /**
     * @retun void
     */
    public function resetData();

}