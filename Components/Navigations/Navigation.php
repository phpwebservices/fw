<?php
/**
 * @author Chris Cunningham
 */

namespace FW\Components\Navigations;

use FW\Components\AbstractComponent;

class Navigation extends AbstractComponent
{
    public function output()
    { ?>
        <div class="navigation"></div><?php
    }

}