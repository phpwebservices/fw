<?php
/**
 * @author Chris Cunningham
 */

namespace FW\Components;

use FW\System\Xml;
use FW\System\Utilities\Traits\MultiInheritance;

/**
 * @method Head|Xml\Element addElement()
 * @method Head|Xml\Element addElementUnique()
 */
class Head extends AbstractComponent
{
    const FORCE_UPDATE = true;

    use MultiInheritance;

    /**
     * @param string $componentsCollectionData
     */
    public function __construct($componentsCollectionData = null)
    {
        $this->inherit(new Xml\Element('head'));
    }

    /**
     * @abstract Root\BaseComponent output()
     * @see Xml\Element output()
     */
    public function output()
    {
        $this->__call(__FUNCTION__, []);
    }

    /**
     * PRIVATE METHODS

     */

    /**
     * @param array   $attributes
     * @param boolean $useEndTag
     *
     * @return Head|Xml\Element
     */
    private function addMeta(array $attributes, $useEndTag = false)
    {
        return $this->addElement('meta', $attributes, null, $useEndTag);
    }

    /**
     * @param array   $attributes
     * @param array   $attributesUnique
     * @param boolean $useEndTag
     *
     * @return Head|Xml\Element
     */
    private function addMetaUnique(array $attributes, array $attributesUnique = [], $useEndTag = false)
    {
        return $this->addElementUnique('meta', $attributes, null, $attributesUnique, $useEndTag);
    }

    /**
     * @param string $encoding
     *
     * @return Head|Xml\Element
     */
    private function addEncoding($encoding)
    {
        return $this->addMetaUnique(['http-equiv' => 'Content-Type', 'content' => "text/html; charset={$encoding}"],
            ['charset'], !Xml\Element::USE_END_TAG);
    }

    /**
     * @param array $attributes
     * @param array $attributesUnique
     *
     * @return Head|Xml\Element
     */
    private function addLink(array $attributes, array $attributesUnique = [])
    {
        return !$attributesUnique
            ? $this->addElement('link', $attributes)
            : $this->addElementUnique('link', $attributes, null, $attributesUnique);
    }

    /**
     * PUBLIC METHODS

     */

    /**
     * @param string $title
     *
     * @return Head|Xml\Element
     */
    public function title($title = '')
    {
        return $this->addElementUnique('title', [], $title);
    }

    /**
     * @return Head|Xml\Element
     */
    public function addUTF8()
    {
        return $this->addEncoding('UTF-8');
    }

    /**
     * @return Head|Xml\Element
     */
    public function addISO8859_1()
    {
        return $this->addEncoding('ISO-8859-1');
    }

    /**
     * @param mixed $timeData
     *
     * @return Head|Xml\Element
     */
    public function addLastModified($timeData)
    {
        $time = is_string($timeData) && !is_numeric($timeData) ? strtotime($timeData) : $timeData;
        return $this->addMetaUnique(['http-equiv' => 'last-modified', 'content' => date("D, j M Y H:i:s T", $time)],
            ['http-equiv' => 'last-modified']);
    }

    /**
     * @param string $baseUrl
     *
     * @return Head|Xml\Element
     */
    public function addBaseUrl($baseUrl)
    {
        return $this->addElementUnique('base', ['href' => $baseUrl]);
    }

    /**
     * @param string $keyWords
     *
     * @return Head|Xml\Element
     */
    public function addKeywords($keyWords = '')
    {
        if (!$keyWords) {
            return $this;
        }
        return $this->addMeta(['name' => 'keywords', 'content' => $keyWords]);
    }

    /**
     * @param string $description
     *
     * @return Head|Xml\Element
     */
    public function addDescription($description = '')
    {
        if (!$description) {
            return $this;
        }
        return $this->addMeta(['name' => 'description', 'content' => $description]);
    }

    /**
     * @param string $author
     *
     * @return Head|Xml\Element
     */
    public function addAuthor($author = 'Chris Cunningham')
    {
        return $this->addMeta(['name' => 'author', 'content' => $author]);
    }

    public function addFavicon($filePath = 'favicon.ico')
    {
        $this->addLink(['rel' => 'shortcut icon', 'href' => $filePath, 'type' => 'image/x-icon']);
        $this->addLink(['rel' => 'icon', 'href' => $filePath, 'type' => 'image/x-icon']);
        return $this;
    }

    const ROBOTS_INDEX = 'index';
    const ROBOTS_NOINDEX = 'noindex';

    const ROBOTS_FOLLOW = 'follow';
    const ROBOTS_NOFOLLOW = 'nofollow';

    /**
     * @param string $index
     * @param string $follow
     * @param string $revisit
     *
     * @return Head|Xml\Element
     */
    public function addRobots($index = self::ROBOTS_INDEX, $follow = self::ROBOTS_FOLLOW, $revisit = '7 days')
    {
        $this->addMeta(['name' => 'googlebot', 'content' => "{$index}, {$follow}"]);
        $this->addMeta(['name' => 'robots', 'content' => "{$index}, {$follow}"]);
        $this->addMeta(['name' => 'revisit-after', 'content' => $revisit]);
        return $this;
    }

    /**
     * @param string $fontName
     *
     * @return Head|Xml\Element
     */
    public function addGoogleFont($fontName = '')
    {
        if (!$fontName) {
            return $this;
        }
        $fontName = urlencode($fontName);
        return $this->addCssFullPath(fwGetScheme() . "://fonts.googleapis.com/css?family={$fontName}");
    }

    /**
     * @param string $copyright
     *
     * @return Head|Xml\Element
     */
    public function addCopyright($copyright = '')
    {
        if (!$copyright) {
            return $this;
        }
        return $this->addMeta(['name' => 'copyright', 'content' => $copyright]);
    }

    /**
     * @param string $region
     * @param string $placeName
     * @param string $position
     *
     * @return Head|Xml\Element
     */
    public function addLocation($region = 'GB-GRE', $placeName = 'London', $position = '51.507327,-0.127673')
    {
        $this->addMeta(['name' => 'geo.region', 'content' => $region]);
        $this->addMeta(['name' => 'geo.placename', 'content' => $placeName]);
        $this->addMeta(['name' => 'geo.position', 'content' => $position]);
        return $this;
    }

    /**
     * @param string $cssPath
     *
     * @return Head|Xml\Element
     */
    public function addCss($cssPath)
    {
        return $this->addCssFullPath(\Context::i()->baseUrl() . '/css/' . $cssPath); // Note: Relative urls.
    }

    /**
     * @param string $cssPath
     *
     * @return Head|Xml\Element
     */
    public function addCssFullPath($cssPath)
    {
        return $this->addLink(['rel' => 'stylesheet', 'type' => 'text/css', 'href' => $cssPath]);
    }

    /**
     * @param string $canonical
     *
     * @return Head|Xml\Element
     */
    public function addCanonical($canonical)
    {
        return $this->addLink(['rel' => 'canonical', 'href' => fwUrlEncode(strtolower($canonical))],
            ['rel' => 'canonical']);
    }

    /**
     * @return Head|Xml\Element
     */
    public function addJQuery()
    {
        return $this->addJSScript('js/src/jquery/jquery.min.js');
    }

    /**
     * @return Head|Xml\Element
     */
    public function addFrameWorkCoreJS()
    {
        return $this->addJSScript('js/FWCore.js');
    }

    /**
     * @return Head|Xml\Element
     */
    public function addFrameWorkSpaceJS()
    {
        return $this->addJSScript('js/FWSpace.js');
    }

    /**
     * @param string $scriptPath
     *
     * @return Head|Xml\Element
     */
    public function addJSScript($scriptPath = '')
    {
        if (!$scriptPath) {
            return $this;
        }
        return $this->addElement('script', ['type' => 'text/javascript', 'src' => $scriptPath], '',
            Xml\Element::USE_END_TAG);
    }

    /**
     * @param string $script
     *
     * @return Head|Xml\Element
     */
    public function addJSRaw($script = '')
    {
        if (!$script) {
            return $this;
        }
        return $this->addElement('script', ['type' => 'text/javascript'], $script);
    }

    /**
     * @param $verificationCode
     *
     * @return Head|Xml\Element
     */
    public function addGoogleVerificationCode($verificationCode)
    {
        return $this->addMeta(['name' => 'google-site-verification', 'content' => $verificationCode]);
    }

    /**
     * @param string $trackingCode
     *
     * @return Head|Xml\Element
     */
    public function addGoogleAnalytics($trackingCode = '')
    {
        if (!$trackingCode) {
            return $this;
        }
        return $this->addJSRaw("
                var _gaq = _gaq || [];
                var pluginUrl = '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
                _gaq.push(['_require', 'inpage_linkid', pluginUrl]);
                _gaq.push(['_setAccount', '{$trackingCode}']);
                _gaq.push(['_trackPageview']);

                (function() {
                    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
                })();
            ");
    }

    /**
     * @param string $trackingCode
     * @param string $domain
     *
     * @return $this|Head|Xml\Element
     */
    public function addGoogleUniversalAnalytics($trackingCode = '', $domain = '')
    {
        if (!$trackingCode) {
            return $this;
        }
        return $this->addJSRaw("
                (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
                ga('create', '{$trackingCode}', '{$domain}');
                ga('send', 'pageview');
            ");
    }

}