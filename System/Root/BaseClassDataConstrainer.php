<?php
/**
 * @author Chris Cunningham
 */

namespace FW\System\Root;

use FW\System\Error;

class BaseClassDataConstrainer extends BaseClassStrict
{
    protected $mandatoryData = [];

    public function data(array $data = [])
    {
        $temp = $data;
        foreach ($this->mandatoryData as $key => $valueData) {
            if (!array_key_exists($key, $temp) || is_null($temp[$key])) {
                throw new Error\Exception(get_class($this) . "::data() - Mandatory parameter is missing {$key}",
                    Error\Exception::PROGRAMMER_NOTICE
                );
            } else {
                $this->set($key, $temp[$key]);
                array_shift($temp);
            }
        }
        parent::data($temp);

    }

}