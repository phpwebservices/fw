<?php
/**
 * @author Chris Cunningham
 */

namespace FW\System\Utilities\Traits;

trait Instance
{
    protected static $instance;

    public static function i()
    {
        return (new \ReflectionClass(get_called_class()))->newInstanceArgs(func_get_args());
    }
}

trait Singleton
{
    /**
     * @var static
     */
    protected static $instance;

    public static function i()
    {
        if (!static::$instance instanceof static) {
            static::$instance = (new \ReflectionClass(get_called_class()))->newInstanceArgs(func_get_args());
        }
        return static::$instance;
    }
}

// /** @property DataBaseHandler[] $instance */

/**
 * Only makes sense to use this on very large objects
 */
trait SingletonCache
{
    /**
     * @var static
     */
    protected static $instance;

    protected static $cacheKey = '';

    protected static $cacheTime = 0;

    public static function getCacheTime()
    {
        return static::$cacheTime;
    }

    public static function setCacheTime($cacheTime)
    {
        return static::$cacheTime = $cacheTime;
    }

    public static function getCacheKey()
    {
        if (!static::$cacheKey) {
            static::$cacheKey = stripcslashes(__CLASS__);
        }
        return static::$cacheKey;
    }

    public static function i()
    {
        if (isset(\Settings::i()->modules()['MemoryCache']->isEnabled) && \Settings::i()->modules()['MemoryCache']->isEnabled) {
            $memoryCache = \MemoryCache::i();
            $cacheKey = static::getCacheKey();
            if (!(static::$instance = $memoryCache->get($cacheKey)) instanceof static) {
                $memoryCache->set($cacheKey, (static::$instance = new static()), MEMCACHE_COMPRESSED, static::getCacheTime());
            }
            return static::$instance;
        }
        if (!static::$instance instanceof static) {
            static::$instance = new static;
        }
        return static::$instance;
    }

    public static function updateCache($cacheTime = null)
    {
        \MemoryCache::i()->set(static::getCacheKey(), static::$instance, MEMCACHE_COMPRESSED,
            $cacheTime ? $cacheTime : static::getCacheTime());
    }
}
		
