<?php
/**
 * @author Chris Cunningham
 */

namespace FW\System\Root;

use FW\System\Error;

abstract class ObjectCollection extends Collection
{
    const INSERT_TYPE_BEFORE = 1;
    const INSERT_TYPE_AFTER = 2;

    /**
     * @param $object
     *
     * @return $this
     */
    protected function addObject($object)
    {
        parent::addData($object);
        return $this;
    }

    /**
     * @param string $objectNameSpaceName
     * @param        $newObject
     *
     * @return $this
     */
    protected function insertObjectBefore($objectNameSpaceName, $newObject)
    {
        return $this->insertObject($objectNameSpaceName, $newObject, self::INSERT_TYPE_BEFORE);
    }

    /**
     * @param $objectNameSpaceName
     * @param $newObject
     *
     * @return $this
     */
    protected function insertObjectAfter($objectNameSpaceName, $newObject)
    {
        return $this->insertObject($objectNameSpaceName, $newObject, self::INSERT_TYPE_AFTER);
    }

    /**
     * @param string $objectNamesSpaceName
     * @param bool   $removeAndReIndex
     *
     * @return bool|Ambigous
     */
    public function getObject($objectNamesSpaceName, $removeAndReIndex = false)
    {
        foreach ($this as $object) {
            if (strpos(get_class($object), $objectNamesSpaceName) !== false) {
                if ($removeAndReIndex) {
                    return $this->removeCurrent();
                } else {
                    return $object;
                }
            }
        }
        return false;
    }

    /**
     * @param string    $objectNameSpaceName
     * @param BaseClass $newObject
     * @param null      $insertType
     *
     * @return $this
     */
    private function insertObject($objectNameSpaceName, BaseClass $newObject, $insertType = null)
    {
        $temp = [];
        foreach ($this as $object) {
            switch ($insertType) {
                case self::INSERT_TYPE_BEFORE:
                    if ($objectNameSpaceName != get_class($object)) {
                        continue;
                    }
                    $temp[] = $newObject;
                    $temp[] = $object;
                    break;
                case self::INSERT_TYPE_AFTER:
                    if ($objectNameSpaceName != get_class($object)) {
                        continue;
                    }
                    $temp[] = $object;
                    $temp[] = $newObject;
                default:
                    $temp[] = $object;
                    break;
            }
        }
        $this->iteratorData = $temp;
        return $this;
    }

    /**
     * @param $newObject
     *
     * @return bool
     * @throws \FW\System\Error\Exception
     */
    public function inCollection($newObject)
    {
        $newObject = get_class($newObject);
        $objectAlreadyExists = false;
        if (!array_walk($this, function ($object) use (
            $newObject,
            &$objectAlreadyExists
        ) { // Just wanted to experiment with doing this with closures for loop would be fine also
            if ($object instanceof $newObject) {
                $objectAlreadyExists = true;
                return;
            }
        })
        ) {
            throw new Error\Exception(get_class($this) . ":inCollection() - Failed to array_walk $newObject - Data: " . print_r($this,
                    true));
        };
        return $objectAlreadyExists;
    }

}