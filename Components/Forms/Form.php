<?php
/**
 * @author Chris Cunningham
 */

namespace FW\Components\Forms;

use FW\System\Root;
use FW\System\Error;
use FW\System\Xml;
use FW\System\Utilities\Traits\MultiInheritance;

/**
 * @method Form|Xml\Element addElement()
 * @method Form|Xml\Element attr()
 */
class Form extends Root\BaseComponent
{
    use MultiInheritance;

    const ACCEPT_CHARSET_UTF8 = 'UTF-8';
    const ACCEPT_CHARSET_ISO8859_1 = 'ISO-8859-1';

    const ENC_URLENCODED = 'application/x-www-form-urlencoded';
    const ENC_MULTIPART = 'multipart/form-data';
    const ENC_PLAIN = 'text/plain';

    const METHOD_POST = 'post';
    const METHOD_GET = 'get';

    /**
     * @var array
     */
    protected $attributes = [
        'action' => '',
        'accept-charset' => self::ACCEPT_CHARSET_UTF8,
        'enctype' => self::ENC_URLENCODED,
        'method' => self::METHOD_POST,
        'name' => 'Form',
    ];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        if ($attributes) {
            $this->attributes = array_merge($this->attributes, $attributes);
        }
        $this->inherit(new Xml\Element('form', $this->attributes));
    }

    /**
     * @abstract Root\BaseComponent output()
     * @see Xml\Element output()
     */
    public function output()
    {
        $this->__call(__FUNCTION__, []);
    }

    /**
     * @param string $name
     *
     * @return Form|Xml\Element
     */
    public function addName($name = '')
    {
        $this->attr('name', $name);
        return $this;
    }

    /**
     * @param string $action
     *
     * @return Form|Xml\Element
     */
    public function addAction($action = '')
    {
        if (!$action) {
            $action = \Context::i()->stringRelative();
        }
        $this->attr('action', fwUrlEncode($action));
        return $this;
    }

    /**
     * @param string $value
     *
     * @return Form|Xml\Element
     */
    public function addEnctype($value = self::ENC_URLENCODED)
    {
        $this->attr('enctype', $value);
        return $this;
    }

    /**
     * @param string $value
     * @param string $name
     * @param array  $attributes
     *
     * @return Form|Xml\Element
     */
    public function addSubmit($value = null, $name = null, array $attributes = [])
    {
        return $this->addElement('input',
            array_merge($attributes, ['value' => $value, 'name' => $name, 'type' => 'submit']));
    }

    /**
     * @param string $value
     * @param string $name
     * @param array  $attributes
     *
     * @return Form|Xml\Element
     */
    public function addInput($value = null, $name = null, array $attributes = [])
    {
        return $this->addElement('input', array_merge($attributes, ['value' => $value, 'name' => $name]));
    }

    public function addLoginName()
    {
        return $this; //TODO::chrisc Complete and move to a child class of this class which will be specific for handling login
    }

    public function addPassword()
    {
        return $this; //TODO::chrisc Complete and move to a child class of this class which will be specific for handling login
    }

    public function addEmail($email)
    {
        return $this; //TODO::chrisc Complete and move to a child class of this class which will be specific for handling login
    }

}