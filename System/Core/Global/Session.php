<?php
/**
 * @author Chris Cunningham
 */

// Global Class

use FW\System\Root;
use FW\System\Error;

class SessionException extends Error\Exception
{
}

/**
 * @method static \Session i()
 */
final class Session
{
    /**
     * @var \Session
     * @return \Session
     */
    use \FW\System\Utilities\Traits\Singleton;

    private static $useDataBaseSession = false;

    public static function setUseDataBaseSession($useDataBaseSession)
    {
        self::$useDataBaseSession = $useDataBaseSession;
    }

    public function __construct()
    {
        if (self::$useDataBaseSession) {
            // TODO:: Use Database Session SessionDataBaseHandler::
        }
        session_start();
    }

    public function set($key, $dataData)
    {
        $_SESSION[$key] = is_object($dataData) || is_array($dataData) ? serialize($dataData) : $dataData;
    }

    public function get($key, $defaultData = null)
    {
        if (isset($_SESSION[$key])) {
            if (($dataData = @unserialize($_SESSION[$key])) === false) {
                return $_SESSION[$key];
            }
            return $dataData;
        }
        return $defaultData;
    }

    /**
     * @performance 6% faster than set function on objects and arrays
     *
     * @param string $key
     * @param mixed  $valueData - Object/Array
     */
    public function setSerialize($key, $valueData)
    {
        $_SESSION[$key] = serialize($valueData);
    }

    /**
     * @performance 6% faster than get function on objects and arrays
     *
     * @param string $key
     * @param null   $defaultData
     *
     * @return mixed|null
     */
    public function getSerialize($key, $defaultData = null)
    {
        return isset($_SESSION[$key]) ? unserialize($_SESSION[$key]) : $defaultData;
    }

    public function sessionId()
    {
        return session_id();
    }

    public function __destruct()
    {
        session_write_close();
    }

}