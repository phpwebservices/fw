<?php
/**
 * @author Chris Cunningham
 */

namespace FW\System\Xml;

use FW\System\Root\BaseClass;

use FW\System\Root;
use FW\System\Utilities\Traits\MultiInheritance;

/**
 * @author Chris Cunningham
 */
class Element extends Root\BaseClass
{
    /**
     *
     */
    use MultiInheritance;

    /**
     * The Element tag name example <{$this->name}>{$this->valueData}</{$this->name}>
     * @var string
     */
    protected $name = '';

    /**
     * @var string
     */
    protected $value = null;

    /**
     * @var bool
     */
    protected $useEndTag = false;

    /**
     * @var array
     */
    protected $attributes = [];

    /**
     * @var array
     */
    protected $elements = [];

    /**
     * @var array
     */
    protected $lines = [];

    /**
     * @var array
     */
    protected $output = '';

    const UNIQUE = true;
    const USE_END_TAG = true;

    const ATTR_OVERWRITE = 1;
    const ATTR_APPEND = 2;

    /**
     * @var boolean
     */
    protected $isUnique = false;

    /**
     * @var array
     * @example array = ['rel' => 'canonical']; // Will make the element unique by attr = rel and value = canonical
     */
    protected $attributesUnique = [];

    /**
     * @param string  $name
     * @param array   $defaultAttributes
     * @param mixed   $valueData
     * @param bool    $isUnique
     * @param array   $attributesUnique
     * @param boolean $useEndTag
     */
    public function __construct($name, array $defaultAttributes = [], $valueData = null, $isUnique = false, array $attributesUnique = [], $useEndTag = false)
    {
        $this->name = $name;
        $this->value = (string)$valueData;
        $this->isUnique = $isUnique;
        $this->attributesUnique = $attributesUnique;
        if ($defaultAttributes) {
            $this->attr($defaultAttributes);
        }
        $this->useEndTag = $useEndTag;
    }

    /**
     * @return boolean
     */
    public function isUnique()
    {
        return $this->isUnique;
    }

    /**
     * @return string
     */
    public function getOutput()
    {
        $startTag = "<{$this->name}{$this->attrString()}";
        if (!$this->elements) {
            if ($this->useEndTag) {
                return "{$startTag}>{$this->value}</{$this->name}>";
            }
            return $this->value ? "{$startTag}>{$this->value}</{$this->name}>" : "{$startTag} />";
        }

        $this->lines[] = "{$startTag}>";
        $this->appendElementOutput($this->lines, $this->elements);
        $this->lines[] = "</{$this->name}>";

        return implode(PHP_EOL, $this->lines);
    }

    /**
     * @param string  $name
     * @param string  $valueData
     * @param array   $attributes
     * @param boolean $isUnique @example boolean = true - Element will be unique by tag name
     * @param array   $attributesUnique @example array = ['rel' => 'canonical']; // Will make the element unique by attr = rel and value = canonical
     * @param boolean $useEndTag
     *
     * @return BaseClass - returns an instance of the class that is multi inheriting this class
     */
    private function element($name, array $attributes = [], $valueData = null, $isUnique = false, array $attributesUnique = [], $useEndTag = false)
    {
        if (!$isUnique) {
            $this->elements[] = new Element($name, $attributes, $valueData, false, [], $useEndTag);
        } else {
            $uniqueName = $name;
            foreach ($attributesUnique as $key => $value) { // @example $attributesUnique = ['rel' => 'canonical']; // Will make the element unique by attr rel and value canonical
                $uniqueName .= $key . $value;
            }
            $uniqueName = md5($uniqueName);
            $this->elements[$uniqueName] = new Element($name, $attributes, $valueData, $isUnique,
                $attributesUnique, $useEndTag);
        }
        return $this->this();
    }

    /**
     * @param string  $name
     * @param array   $attributes
     * @param null    $valueData
     * @param boolean $useEndTag
     *
     * @return BaseClass
     */
    public function addElement($name, array $attributes = [], $valueData = null, $useEndTag = false)
    {
        return $this->element($name, $attributes, $valueData, !self::UNIQUE, [], $useEndTag);
    }

    /**
     * @param string  $name
     * @param array   $attributes
     * @param mixed   $valueData
     * @param array   $attributesUnique
     * @param boolean $useEndTag
     *
     * @return BaseClass
     */
    public function addElementUnique($name, array $attributes = [], $valueData = null, array $attributesUnique = [], $useEndTag = false)
    {
        return $this->element($name, $attributes, $valueData, self::UNIQUE, $attributesUnique, $useEndTag);
    }

    /**
     * @return void
     */
    public function output()
    {
        echo $this->getOutput();
    }

    /**
     * @return boolean
     */
    public function hasElements()
    {
        return (bool)$this->elements;
    }

    /**
     * @return array
     */
    public function getElements()
    {
        return $this->elements;
    }

    /**
     * @return int
     */
    public function getElementsCount()
    {
        return count($this->elements);
    }

    /**
     * @param array $outputLines
     * @param array $elements
     */
    protected function appendElementOutput(&$outputLines, $elements)
    {
        foreach ($elements as $key => $element) {
            /* @var $element Element */
            if (!$element->hasElements()) {
                $outputLines[] = $element->getOutput();
            } else {
                $this->appendElementOutput($outputLines, $element->getElements());
            }
        }
    }

    /**
     * @param mixed  $attributeData
     * @param mixed  $valueData
     * @param int    $optionType
     * @param string $separator
     *
     * @return array|null|string
     */
    public function attr($attributeData = null, $valueData = null, $optionType = self::ATTR_OVERWRITE, $separator = ' ')
    {
        if (is_array($attributeData)) {
            foreach ($attributeData as $attributeName => $attributeValue) {
                if (is_null($attributeValue)) {
                    continue;
                }
                $this->attr($attributeName, $attributeValue, $separator);
            }
            return $this->attributes;
        }
        if (is_null($attributeData) && is_null($valueData)) {
            return $this->attributes;
        }
        if ($optionType === self::ATTR_APPEND && isset($this->attributes[$attributeData]) && strlen($this->attributes[$attributeData])) {
            if (is_null($valueData)) {
                return $this->attributes[$attributeData];
            } else {
                $valueData = "{$this->attributes[$attributeData]}{$separator}$valueData";
            }
        }
        return ($this->attributes[$attributeData] = $valueData);
    }

    /**
     * @return string
     */
    public function attrString()
    {
        $attributes = [];
        foreach ($this->attributes as $attributeName => $attributeValue) {
            if (is_null($attributeValue)) {
                continue;
            }
            $attributes[] = "{$attributeName}='{$attributeValue}'";
        }
        return $attributes ? " " . implode(' ', $attributes) : '';
    }

}