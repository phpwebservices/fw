<?php
/**
 * @author Chris Cunningham
 */

namespace FW;

require 'FW/System/requireFiles.php';

use FW\System\Core;
use FW\System\Root;
use FW\System\Error;
use FW\System\Utilities;
use FW\System\Controller;

final class Engine
{
    /**
     * @return Engine
     */
    public function __construct()
    {
        $settings = null;

        try {
            $settings = \Settings::i(); /* @var $settings \Settings */
            Error\Handler::i($settings);
            Core\AutoLoader::i();
            Core\ModuleLoader::i($settings->modules());
        } catch (\Exception $exception) {
            if (($endExecution = !$settings instanceof \Settings)) {
                Error\Exception::setLogErrorSwitch(true);
            }
            Error\Exception::handleException($exception, $endExecution);
        }
    }

    public function start()
    {
        \Session::i();
        \Request::i();
        Controller\Dispatcher::initiate(
            \Context::i()->stringRelative());
    }
}