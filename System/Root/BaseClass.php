<?php
/**
 * @author Chris Cunningham
 */

namespace FW\System\Root;

use FW\System\Interfaces;

class BaseClass implements Interfaces\DataBean
{
    /**
     * @var array
     */
    protected $data = [];

    /**
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data($data);
    }

    /**
     * Set the objects data from and array of data
     *
     * @param array $data
     *
     * @return array|void
     */
    public function data(array $data = [])
    {
        if (!$data) {
            return $this->data;
        }
        foreach ($data as $key => $valueData) {
            $this->set($key, $valueData);
        }
        return $this->data;
    }

    /**
     * @param string $key
     * @param mixed  $defaultData
     *
     * @return null|mixed
     */
    protected function get($key, $defaultData = null)
    {
        return isset($this->data[$key]) ? $this->data[$key] : $defaultData;
    }

    /**
     * @param $key
     *
     * @return null|mixed
     */
    public function __get($key)
    {
        return $this->get($key);
    }

    /**
     * print the output of  the request data
     *
     * @param string $key
     * @param mixed  $defaultData
     */
    public function out($key, $defaultData = null)
    {
        $dataData = $this->get($key, $defaultData);
        if (is_array($dataData) || is_object($dataData)) {
            print_r($dataData);
        } else {
            print $dataData;
        }
    }

    /**
     * @param string $key
     * @param mixed  $valueData
     */
    protected function set($key, $valueData = null)
    {
        $this->data[$key] = $valueData;
    }

    /**
     * @param string $key
     * @param mixed  $valueData
     */
    public function __set($key, $valueData = null)
    {
        $this->set($key, $valueData);
    }

    /**
     * Check for the existence of a param.
     * @example All of these return true /context/?search= AND /context/?search=1 AND /context/?search=0
     *
     * @param string $key
     *
     * @return boolean
     */
    public function exists($key)
    {
        return array_key_exists($key, $this->data);
    }

    /**
     * @param string $key
     */
    public function remove($key)
    {
        unset($this->data[$key]);
    }

    /**
     * @param array $data
     */
    public function replaceData(array $data = [])
    {
        $this->data = $data;
    }

    /**
     * @return void
     */
    public function resetData()
    {
        $this->data = [];
    }

    /**
     * For aggregate relationship type data - The data can
     * persist to exists once this object instance is destroyed
     *
     * @param array $data
     */
    public function setAggregateData(array &$data = [])
    {
        $this->data =& $data;
    }

    /**
     * returns the short class name of the calling/child class name
     * @example \NameSpace\ClassName will return ClassName
     * @return string
     */
    public function classShortName()
    {
        $classes = explode('\\', get_called_class());
        return end($classes);
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->data);
    }
}