<?php
/**
 * @author Chris Cunningham
 */

namespace FW\System\Utilities\Traits;

trait MultiInheritance
{
    /**
     * Is a type of A and type B class
     * @var array
     */
    protected $multiInheritanceInstances = [];

    /**
     * The following methods make it possible for a style of multi inheritance
     * Multi inheritance classes have there functions over written by preceding multi inheritance classes defined
     * using inherit() method.
     * In other words the first object it finds in $this->multiInheritanceInstances that has a method with the name you are trying to call
     * is the one that will be used.
     * This is not really the best idea to use multi inheritance I just wanted to make it for fun to show it is possible.
     * For example:
     * class MainClass {
     *     public function __construct() {
     *         $this->inherit(new CClass)->inherit(new BClass)->inherit(new AClass);
     *     }
     * }
     * class AClass {
     *     public function output() {echo 'AClass Method output';}
     * }
     * class BClass {
     *     public function output() {echo 'BClass Method output';}
     * }
     * class CClass {
     *     public function draw() {echo 'CClass Method draw';}
     * }
     * $mainClass = new MainClass;
     * $mainClass->output(); // outputs: BClass Method output
     * $mainClass->draw();   // outputs: CClass Method draw
     * !!! Note to self: Already considered and implemented multi inheritance using traits but have reverted the code and
     * decided against it due to property conflict resolution. and __constructor over riding issues.
     * Will reconsider this once php sort out the bugs of __constructors
     * Example #12 Conflict Resolution Form @see http://php.net/manual/en/language.oop5.traits.php
     * If a trait defines a property then a class can not define a property with the same name, otherwise an error is
     * issued. It is an E_STRICT if the class definition is compatible (same visibility and initial value) or fatal
     * error otherwise. (ChrisC - I do not want this Behavior!!!)

     */

    /**
     * @param object $object
     * @param string $instanceName
     *
     * @throws \Exception
     * @return BaseClass
     */
    public function inherit($object, $instanceName = '')
    {
        if (!is_object($object)) {
            throw new \Exception(get_called_class() . "->inherit() - No object passed. " . print_r($object));
        }
        $this->multiInheritanceInstances[get_class($this)][$instanceName ? $instanceName : get_class($object)] = $object;
        return $this;
    }

    /**
     * @var \FW\System\Root\BaseClass
     */
    protected $thisCalledInstance = null;

    /**
     * @param $object
     */
    public function setThisCalledInstance($object)
    {
        $this->thisCalledInstance = $object;
    }

    /**
     * @return \FW\System\Root\BaseClass - Or instance of class that is multi inheriting from a class that is extending this
     */
    public function this()
    {
        return method_exists($this->thisCalledInstance, 'inherit') ? $this->thisCalledInstance : $this;
    }

    /**
     * Use to handle multi inheritance class models.
     *
     * @param string $methodName
     * @param array  $arguments
     *
     * @throws \Exception
     * @return mixed
     */
    public function __call($methodName, $arguments)
    {
        $calledClass = get_called_class();
        $thisClass = get_class($this);

        if (!isset($this->multiInheritanceInstances[$thisClass])) {
            $reflection = new \ReflectionClass($thisClass);
            if (!$reflection->getMethod($methodName)->isPublic()) {
                throw new \Exception("{$calledClass}->{$methodName}() - Trying to call a non public method");
            }
            throw new \Exception("{$calledClass}->{$methodName}() - Class not declared for multi inheritance check that the method you are calling exists in your class");
        }

        foreach ($this->multiInheritanceInstances[$thisClass] as $object) {
            if (method_exists($object, $methodName)) {
                if (!method_exists($object, 'setThisCalledInstance')) {
                    throw new \Exception(get_called_class() . "->{$methodName}() - Only FW classes using the multi inheritance trait can be used with multi inheritance");
                }
                $object->setThisCalledInstance($this);
                return call_user_func_array([$object, $methodName], $arguments);
            }
        }
        throw new \Exception("{$calledClass}->{$methodName}() - Method does not exist. parameters: " . print_r($arguments,
                true));
    }

}