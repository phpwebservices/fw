<?php
/**
 * @author Chris Cunningham
 */

namespace FW\System\Core;

use FW\System\Error;

use FW\System\Interfaces;

class ModuleLoader
{
    public static function i(array $modules)
    {
        return new self($modules);
    }

    protected function __construct(array $modules)
    {
        /**
         * @var $moduleClassName Interfaces\ModuleSettings
         * @var $moduleSettings \stdClass
         */
        foreach ($modules as $moduleClassName => $moduleSettings) {
            if (!class_exists($moduleClassName)) {
                throw new Error\Exception("Class doesn't exist {$moduleClassName} check your FW.ini file");
            }
            $moduleClassName::initiate($moduleSettings);
        }
    }

}