<?php
/**
 * @author Chris Cunningham
 */

namespace FW\Components\Media\Video;

use FW\System\Root;
use FW\Components\AbstractComponent;

class Flash extends AbstractComponent
{
    public $url = '';
    protected $width = 0;
    protected $height = 0;

    public function __construct($url, $width = null, $height = null)
    {
        $this->url = $url;
        $this->width = $width ?: '100%';
        $this->height = $height ?: '100%';
    }

    public function output()
    { ?>
    <object width="<?= $this->width; ?>" height="<?= $this->height; ?>">
        <param name="movie" value="<?= $this->url; ?>"/>
        <param name="allowFullScreen" value="true"/>
        <param name="allowScriptAccess" value="always"/>
        <embed class="shadow" src="<?= $this->url; ?>" type="application/x-shockwave-flash" allowfullscreen="true"
               allowScriptAccess="always" width="<?= $this->width; ?>" height="<?= $this->height; ?>"></embed>
        </object><?php
    }

}