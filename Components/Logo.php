<?php
/**
 * @author Chris Cunningham
 */

namespace FW\Components;

use FW\System\Root;
use FW\Components\AbstractComponent;

class Logo extends AbstractComponent
{
    public function output()
    { ?>
        <div class="logo">Logo Image Here<img alt="Logo" src="images/logo.png"/></div><?php
    }

}