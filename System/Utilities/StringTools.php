<?php
/**
 * Created by PhpStorm.
 * User: chriscunningham
 * Date: 22/02/2014
 * Time: 00:41
 */

use FW\System\Error;

class StringTools
{
    /**
     * @param string $parameter
     *
     * @return mixed|string
     */
    public static function filterGetSafeData($parameter)
    {
        return preg_replace('/[^\w\d\s-_:,{}\[\]\'\"\/%\+\.]/i', '', $parameter);
    }

}