<?php
/**
 * @author Chris Cunningham
 */

namespace FW\System\Root;

class Iterator implements \Iterator
{
    protected $index = 0;

    protected $iteratorData = [];

    public function rewind()
    {
        $this->index = 0;
    }

    public function current()
    {
        return $this->iteratorData[$this->index];
    }

    public function key()
    {
        return $this->index;
    }

    public function next()
    {
        $this->index++;
    }

    public function valid()
    {
        return isset($this->iteratorData[$this->index]);
    }

    public function count()
    {
        return count($this->iteratorData);
    }

    public function prev()
    {
        $this->index--;
    }

}