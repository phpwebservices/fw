<?php
/**
 * @author Chris Cunningham
 */

namespace FW\Components\User;

use FW\Components\AbstractComponent;

use FW\Components\Forms\Form;

use FW\System\Actions;
use FW\System\Actions\Abstracted\Action;

class Login extends AbstractComponent
{
    public function __construct()
    {
        parent::__construct([
            (new Form)
                ->addName('LoginForm')
                ->addAction()
                ->addEnctype()
                ->addLoginName()
                ->addPassword()
                ->addInput('some value')
                ->addSubmit('Click Me')
        ]);
    }

    public function output()
    { ?>
        <div class="login">
        <?php echo $this->getComponentsOutput(); ?>
        </div><?php
    }

    public static function actions()
    {
        return new LoginDefaultAction;
    }
}

class LoginDefaultAction extends Action
{
    public function run()
    {
        $this->add(new TestAction);
        if (\Request::i()->exists('test')) {
            # echo 'Testin';
        }
    }
}

class TestAction extends Action
{
    public function run()
    {
        # echo 'Test Action';
    }

}