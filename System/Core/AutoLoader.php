<?php
/**
 * @author Chris Cunningham
 */

namespace FW\System\Core;

class AutoLoader
{
    public static function i()
    {
        return new self;
    }

    protected function __construct(array $data = [])
    {
        spl_autoload_register([$this, 'autoLoadRegister'], true);
    }

    private function autoLoadRegister($className)
    {
        if ($className[0] == '\\') {
            $className = substr($className, 1);
        }
        if ($this->loadClasses($className) || $this->loadCoreClasses($className)) {
            return;
        }
    }

    /**
     * @var ['sDirectory' => 'sFileTypePrefix'];
     */
    protected $classLocations = [
        'FW' => '',
        'Encryption/SSL' => '',
        'Controllers' => '.c',
        'App/Views' => '.v',
        'Components' => '',
        'App/Components' => '',
    ];

    // TODO:: This should throw an exception if the object doesn't extend BaseClass
    private function loadClasses($className)
    {
        $classPath = \Tools::namespaceToDirectoryPath($className);
        foreach ($this->classLocations as $directory => $fileTypePrefix) {
            if (strpos($classPath, $directory) !== false) {
                $filePath = "{$classPath}{$fileTypePrefix}.php";
                if (!file_exists(BASE_PATH . '/' . $filePath)) {
                    if ($fileTypePrefix == '.c') {
                        throw new \ContextException("File doesn't exist: $filePath", \ContextException::STATUS_404);
                    } else {
                        throw new \Exception("File doesn't exist: $filePath");
                    }
                }
                require $filePath;
                return true;
            }
        }
    }

    protected $coreClassLocations = [
        'FW/System/Utilities',
        'FW/System/Core/Global'
    ];

    private function loadCoreClasses($className)
    {
        foreach ($this->coreClassLocations as $classLocation) {
            if (file_exists($filePath = "{$classLocation}/{$className}.php")) {
                require $filePath;
                return true;
            }
        }
    }

}