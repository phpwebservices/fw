<?php
/**
 * Created by PhpStorm.
 * User: chriscunningham
 * Date: 28/12/2013
 * Time: 19:11
 */

namespace FW\Components\Social;

use FW\System\Root;
use FW\Components\AbstractComponent;

class FaceBook extends AbstractComponent
{
    protected $domain = '';
    protected $faceBookHome = '';

    public function setDomain($domain)
    {
        $this->domain = $domain;
    }

    public function setFaceBookHome($faceBookHome)
    {
        $this->faceBookHome = $faceBookHome;
    }

    public function output()
    {
    }

    public function js()
    {
        return <<< JS
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
JS;
    }

    /**
     * Adds a like button that would add a like to the facebook home page of the brands site.
     *
     * @param string $faceBookHome
     *
     * @return string
     */
    public function likeButtonSitePageHome($faceBookHome = '')
    {
        $faceBookHome = $faceBookHome ?: $this->faceBookHome;
        return $this->likeButton("https://www.facebook.com/{$faceBookHome}");
    }

    /**
     * @param string $domain
     *
     * @return string
     */
    public function likeButton($domain = '')
    {
        $domain = $domain ?: $this->domain;
        return <<< HTML
<div class="fb-like" data-href="{$domain}/" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
HTML;
    }

    /**
     * @param string $faceBookHome
     *
     * @return string
     */
    public function followButton($faceBookHome = '')
    {
        $faceBookHome = $faceBookHome ?: $this->faceBookHome;
        return <<< HTML
<div class="fb-follow" data-href="https://www.facebook.com/{$faceBookHome}" data-layout="button_count" data-show-faces="true" data-width="450"></div>
HTML;
    }

}