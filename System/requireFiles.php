<?php
/**
 * This file is used to include all required files for the frame work to run
 * Files are placed in order of precedence
 * @author chriscunningham0
 */
require 'FW/System/Standard.php';
require 'FW/System/defines.php';
require 'FW/System/Utilities/Tools.php';
require 'FW/System/Utilities/StringTools.php';
require 'FW/System/Debug/Debug.php';
require 'FW/System/Interfaces/DataBean.php';
require 'FW/System/Utilities/Traits/Common.php';
require 'FW/System/Utilities/Traits/MultiInheritance.php';
require 'FW/System/Root/BaseClass.php';
require 'FW/System/Root/BaseClassStrict.php';
require 'FW/System/Root/BaseClassDataConstrainer.php';
require 'FW/System/Error/Exception.php';
require 'FW/System/Core/Global/Context.php';
require 'FW/System/Error/Handler.php';
require 'FW/System/Core/AutoLoader.php';
require 'FW/System/Settings.php';