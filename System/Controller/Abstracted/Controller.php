<?php
/**
 * @author Chris Cunningham
 */

namespace FW\System\Controller\Abstracted;

use FW\System\Root;

use FW\System\Actions;
use FW\System\Controller\Dispatcher;
use FW\System\Extension\ReflectionClass;

abstract class Controller extends Root\BaseClass
{
    abstract public function execute();

    /**
     * @var boolean
     */
    protected $isDispatched = false;

    /**
     * @return void
     */
    public function setDispatched()
    {
        $this->isDispatched = true;
    }

    /**
     * @return boolean
     */
    public function isDispatched()
    {
        return $this->isDispatched;
    }

    /**
     * @var boolean
     */
    private $preciseContextMatch = true;

    /**
     * Enable/Disable the ability to not 404 error when url
     * context does n't match the exact controller path. Use
     * when canonical should equal the controller path but
     * additional context levels need to be accessed
     * using \Context::i()->context($level);
     *
     * @param null $preciseContextMatch
     *
     * @return bool|null
     */
    public function preciseContextMatch($preciseContextMatch = null)
    {
        if (is_null($preciseContextMatch)) {
            return $this->preciseContextMatch;
        }
        return ($this->preciseContextMatch = $preciseContextMatch);
    }

    /**
     * @throws \ContextException
     */
    public function executePreciseContextMatch()
    {
        if ($this->preciseContextMatch) {
            $dispatchController = Dispatcher::getLoadedControllerContextArray();

            if ($dispatchController === ($contextArray = \Context::i()->context())) {
                return true;
            }

            if ($dispatchController === array_slice($contextArray, 0, -1)) {
                return true; // make sure we account for xml and json view
            }

            $context = Dispatcher::getLoadedControllerContextString();

            throw new \ContextException("Precise url context match enabled and url
                {$context} context did not match controller context", \ContextException::STATUS_404
            );
        }
    }

    public function extractCustomPublicData($data)
    {
        $temp = [];
        foreach ($data as $key => $valueData) {
            if ($key{0} == '_') {
                continue;
            }
            if (is_object($valueData)) {
                $temp[$key] = ReflectionClass::getPublicProperties($valueData);
            } else {
                if (is_array($valueData)) {
                    $temp[$key] = $this->extractCustomPublicData($valueData);
                } else {
                    $temp[$key] = $valueData;
                }
            }
        }
        return $temp;
    }
}