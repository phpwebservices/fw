<?php
/**
 * @author Chris Cunningham
 */

// Global Class

use FW\System\Root;
use FW\System\Error;

class RequestException extends Error\Exception
{
}

/**
 * @method static \Request i()
 */
class Request extends Root\BaseClass
{
    /**
     * @var \Request
     * @return \Request
     */
    use FW\System\Utilities\Traits\Singleton;

    public function __construct()
    {
        $this->data($this->processRequestData($_REQUEST));
        $this->remove('c'); // Rewrites use parameter c for controller path remove from request object
    }

    protected function processRequestData($data)
    {
        $temp = [];
        foreach ($data as $key => $parameterData) {
            $safeKey = \StringTools::filterGetSafeData($key);
            if (is_array($parameterData)) {
                $temp[$safeKey] = $this->processRequestData($parameterData);
            } else {
                $temp[$safeKey] = \StringTools::filterGetSafeData($parameterData);
            }
        }
        return $temp;
    }

    public function data(array $data = [])
    {
        if (!$data) {
            return $this->data;
        }
        $this->data = $data;
    }

    public function get($key, $defaultData = null)
    {
        return isset($this->data[$key]) ? $this->data[$key] : $defaultData;
    }

    public function set($key, $valueData = null)
    {
        $this->data[$key] = $valueData;
    }

    public function getHttpQueryString()
    {
        return http_build_query($this->data(), null, null, PHP_QUERY_RFC3986);
    }

}