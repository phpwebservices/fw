<?php
/**
 * @author Chris Cunningham
 */

namespace FW\System\Error;

use FW\System;

class Exception extends \Exception
{
    const PROGRAMMER_NOTICE = 1; // Programmer forgot a configuration setting
    const PROGRAMMER_NOTICE_CONNECTION_ERROR = 2; // Failed to connect to service
    const USER_NOTICE = 101; // User forgot to set a configuration setting

    private $backTrace = '';

    public function __construct($message = '', $code = null)
    {
        $this->setBackTrace();
        parent::__construct(fwGetCaller() . " - $message", $code);
    }

    public static function getErrorType($errorCode)
    {
        switch ($errorCode) {
            case E_ERROR: // 1 //
                return 'E_ERROR';
            case E_WARNING: // 2 //
                return 'E_WARNING';
            case E_PARSE: // 4 //
                return 'E_PARSE';
            case E_NOTICE: // 8 //
                return 'E_NOTICE';
            case E_CORE_ERROR: // 16 //
                return 'E_CORE_ERROR';
            case E_CORE_WARNING: // 32 //
                return 'E_CORE_WARNING';
            case E_COMPILE_ERROR: // 64 //
                return 'E_COMPILE_ERROR';
            case E_COMPILE_WARNING: // 128 //
                return 'E_COMPILE_WARNING';
            case E_USER_ERROR: // 256 //
                return 'E_USER_ERROR';
            case E_USER_WARNING: // 512 //
                return 'E_USER_WARNING';
            case E_USER_NOTICE: // 1024 //
                return 'E_USER_NOTICE';
            case E_STRICT: // 2048 //
                return 'E_STRICT';
            case E_RECOVERABLE_ERROR: // 4096 //
                return 'E_RECOVERABLE_ERROR';
            case E_DEPRECATED: // 8192 //
                return 'E_DEPRECATED';
            case E_USER_DEPRECATED: // 16384 //
                return 'E_USER_DEPRECATED';
            case E_ALL: // E_ALL // 32767 //
                return 'E_ALL';
            default: // FW Exception
                return 'FW_EXCEPTION';
        }
    }

    private function setBackTrace()
    {
        ob_start();
        pre($this);
        $this->backTrace = ob_get_clean();
    }

    private function getBackTrace()
    {
        return $this->backTrace;
    }

    public function printDebug()
    {
        echo $this->getBackTrace();
    }

    /**
     * Class Settings
     */

    public static $displayError = false;
    public static $logError = false;
    public static $displayExceptionError = false;
    public static $logExceptionError = false;

    public static function setDisplayErrorSwitch($value = true)
    {
        self::$displayError = $value;
    }

    public static function setLogErrorSwitch($value = true)
    {
        self::$logError = $value;
    }

    public static function setDisplayExceptionErrorSwitch($value = true)
    {
        self::$displayExceptionError = $value;
    }

    public static function setLogExceptionErrorSwitch($valueData = true)
    {
        self::$logExceptionError = $valueData;
    }

    public static function handleException(\Exception $exception, $endExecution = false)
    {
        $referrer = \StringTools::filterGetSafeData(G('HTTP_REFERER', $_SERVER));
        $ip = \StringTools::filterGetSafeData(G('REMOTE_ADDR', $_SERVER));

        $errorMessage = $ip . " : {$referrer}: " . self::getErrorType($exception->getCode()) . ": {$exception->getMessage()}: in {$exception->getFile()} on line {$exception->getLine()}</br>";

        if (self::$displayError) { // PHP Style errors
            print $errorMessage;
        }

        if (self::$logError) { // PHP Style errors logged to log file
            @error_log($errorMessage);
        }

        if (self::$displayExceptionError) {
            if (method_exists($exception, 'printDebug')) {
                $exception->printDebug();
            } else {
                pree($exception);
            }
        }

        if (self::$logExceptionError) {
            @error_log("FW " . var_export($exception, true));
        }

        if ($endExecution || $exception instanceof \ContextException) {
            \Tools::setContentTypeHeader(($type = \Context::i()->last(['xml', 'json'], 'html')));

            if ($type == 'json') {
                exit(json_encode(['error' => $exception->getCode()]));
            }

            $code = $exception->getCode() == 0 ? 500 : $exception->getCode();

            header("HTTP/1.0 {$code} Not Found");
            include "Errors/" . ucfirst($type) . "/{$code}.$type";
            exit();
        }

    }
}