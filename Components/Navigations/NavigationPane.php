<?php
/**
 * @author Chris Cunningham
 */

namespace FW\Components\Navigations;

use FW\Components\AbstractComponent;

class NavigationPane extends AbstractComponent
{
    public function output()
    { ?>
        <div class="navigationPane"></div><?php
    }

}