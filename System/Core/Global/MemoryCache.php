<?php
/**
 * @author Chris Cunningham
 */

use FW\System\Interfaces;

class MemoryCache extends Memcache implements Interfaces\ModuleSettings
{
    const A_NANO_CACHE = 5;
    const B_TINY_CACHE = 15; // 15 secs
    const C_SHORT_CACHE = 60; // 1 min
    const D_SMALL_CACHE = 300; // 5 mins
    const E_MEDIUM_CACHE = 1800; // 30 mins
    const F_LONG_CACHE = 7200; // 2 hours
    const G_DAILY_CACHE = 86400; // 24 hours
    const H_WEEKLY_CACHE = 604800; // 7 Days
    const I_MONTHLY_CACHE = 2629728; // 1 month - 1/12 Of 365.24 Days

    const Z_INFINITE_CACHE = 0; // ∞

    /**
     * @var MemoryCache
     */
    protected static $instance = null;

    /**
     * @param string $host
     * @param string $port
     *
     * @return MemoryCache
     */
    public static function i($host = null, $port = null)
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self;
            self::$instance->connect($host ?: self::$host, $port ?: self::$port);
        }
        return self::$instance;
    }

    /**
     * @var string
     */
    protected static $host = 'localhost';

    /**
     * @var int
     */
    protected static $port = 11211;

    /**
     * @param int $port
     */
    public static function setPort($port)
    {
        self::$port = $port;
    }

    /**
     * @param string $host
     *
     * @return string
     */
    public static function setHost($host)
    {
        self::$host = $host;
    }

    /**
     * @return number
     */
    public function getServerStatus()
    {
        return parent::getServerStatus(self::$host, self::$port);
    }

    /**
     * @param bool $logFlush
     * @param bool $logExtendedStats
     *
     * @return void
     *
     * @see Memcache::flush()
     */
    public function flush($logFlush = false, $logExtendedStats = false)
    {
        $extendedStats = $this->getExtendedStats();
        $status = parent::flush() ? 'Flushed' : 'Flush Failed';
        if ($logFlush) {
            $extendedStats = $logExtendedStats ? var_export($extendedStats, true) : null;
            @error_log("MemoryCache {$status} : {$extendedStats}");
        }
    }

    /**
     * Factory Method
     *
     * @param \stdClass $moduleSettings
     */
    public static function initiate(\stdClass $moduleSettings)
    {
        if (!$moduleSettings->isEnabled) {
            return;
        }
        $memoryCache = \MemoryCache::i(G('Host', $moduleSettings), G('Port', $moduleSettings));
        if (G('Flush', $moduleSettings)) {
            $memoryCache->flush($moduleSettings->LogFlush, $moduleSettings->LogExtendedStats);
        }
    }

}