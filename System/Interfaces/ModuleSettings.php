<?php
/**
 * @author Chris Cunningham
 */

namespace FW\System\Interfaces;

interface ModuleSettings
{
    public static function initiate(\stdClass $moduleSettings);

}