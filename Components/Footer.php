<?php
/**
 * Created by PhpStorm.
 * User: chriscunningham
 * Date: 04/01/2014
 * Time: 18:25
 */

namespace FW\Components;

class Footer extends AbstractComponent
{
    public function output()
    { ?>
        <div class="footer">
        <?php echo '&copy; 2012-' . date('Y'); ?>
        <a title="<?= \Settings::i()->siteName(); ?> - Home"
           href="<?= \Context::url(); ?>"><?= $settings->domain(); ?></a>
        | <?= $this->addPrivacyPolicy(); ?>
        </div><?php
    }

    public function addPrivacyPolicy()
    {
        $privacyPolicyId = \Settings::i()->privacyPolicyId();
        return <<<HTML
<div class="privacyPolicy"><a href="http://www.iubenda.com/privacy-policy/{$privacyPolicyId}" class="iubenda-white no-brand iubenda-embed" title="Privacy Policy">Privacy Policy</a><script type="text/javascript">(function (w,d) {var loader = function () {var s = d.createElement("script"), tag = d.getElementsByTagName("script")[0]; s.src = "http://cdn.iubenda.com/iubenda.js"; tag.parentNode.insertBefore(s,tag);}; if (w.addEventListener){w.addEventListener("load", loader, false);}else if (w.attachEvent){w.attachEvent("onload", loader);}else{w.onload = loader;}})(window, document);</script></div>
HTML;
    }

}