<?php
/**
 * @author Chris Cunningham
 */

namespace FW\Models\DataHandlers;

use FW\System\Root;
use FW\System\Utilities\Traits;

/**
 * Class Items
 * @package FW\Models\DataHandlers
 */
class Items extends Root\DataBaseHandler
{
    protected $tableName = 'Items';

    /**
     * @param string $columnName
     * @param mixed  $value
     * @param int    $type
     *
     * @return \FW\Models\DataHandlers\Item
     */
    public function selectOne($columnName, $value, $type = \PDO::FETCH_CLASS)
    {
        return parent::selectOne($columnName, $value, $type);
    }
}

/**
 * Class Item
 *
 * @property-read integer $id
 * @property-read string $hash
 * @property-read string $context
 * @property-read string $author
 * @property-read string $title
 * @property-read string $date timestamp
 * @property-read string $text text
 *
 * @package FW\Models\DataHandlers
 */
class Item extends \stdClass
{
}