<?php
/**
 * @author Chris Cunningham
 */

namespace FW\System\Root;

use FW\System\Interfaces;

class View extends BaseClass implements Interfaces\View
{
    public function content()
    {
    }

}