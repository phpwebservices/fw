<?php
/**
 * @author Chris Cunningham
 */

namespace FW\System\Controller\Abstracted;

use App\Views;
use FW\Components\Collections\DefaultComponents;

use FW\System\Extension\ReflectionClass;

use FW\Components;
use FW\System\Error\Exception;
use FW\System\Root;

abstract class Display extends Controller
{
    protected $components = null;

    public function __construct()
    {
        $this->components(new DefaultComponents);
    }

    /**
     * @var array $nameSpacePrefix
     */
    private static $nameSpacePrefix = ['App', 'Views'];

    /**
     * @param array $nameSpacePrefix
     */
    public static function setNameSpacePrefix(array $nameSpacePrefix)
    {
        self::$nameSpacePrefix = $nameSpacePrefix;
    }

    /**
     * @return array
     */
    public static function getNameSpacePrefixArray()
    {
        return self::$nameSpacePrefix;
    }

    /**
     * @return string - App\Controllers\Main
     */
    public static function getNameSpacePrefixString()
    {
        return implode('\\', self::getNameSpacePrefixArray());
    }

    /**
     * @param Components\AbstractCollection $components
     *
     * @return Components\AbstractCollection|\FW\Components\Collections\DefaultComponents|\App\Components\Collections\MainComponents
     */
    public function components(Components\AbstractCollection $components = null)
    {
        if (!is_null($components)) {
            $this->components = $components;
        }
        return $this->components;
    }

    public function outputComponents()
    {
        if (\Context::i()->last('json')) {
            $this->printOutputJson();
        }

        if (\Context::i()->last('xml')) {
            $this->printOutputXml();
        }

        if (\Context::i()->last('callxml')) {
            $this->printOutputCallXml();
        }

        if (\Context::i()->last('ccxml')) {
            $this->printOutputCCXml();
        }

        $viewClassName = implode('\\', array_replace(explode('\\', get_called_class()), self::getNameSpacePrefixArray()));

        $this->components()->Content(new $viewClassName);

        $data = $this->data();

        if (!\Settings::i()->useTidyHtml()) {
            return $this->components()->printOutput($data);
        }
        return \HtmlTools::printTidyHtml($this->components()->getOutput($data));
    }

    private function printOutputJson()
    {
        \Tools::setContentTypeHeader('json');
        exit(json_encode($this->extractCustomPublicData($this->data()), JSON_PRETTY_PRINT));
    }

    private function printOutputXml()
    {
        $xml = new \SimpleXMLElement('<rsp stat="ok"/>');
        $xml->addAttribute('encoding', 'UTF-8');
        $this->buildXml($xml, $this->extractCustomPublicData($this->data()));

        \Tools::setContentTypeHeader('xml');
        exit($xml->asXML());
    }

    private function printOutputCallXml()
    {
        $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8" ?><callxml version="3.0"></callxml>');

        $this->buildXml($xml, $this->extractCustomPublicData($this->data()));

        \Tools::setContentTypeHeader('xml');
        exit($xml->asXML());
    }

    private function printOutputCCXml()
    {
        $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8" ?><ccxml version="3.0"></ccxml>');

        $this->buildXml($xml, $this->extractCustomPublicData($this->data()));

        \Tools::setContentTypeHeader('xml');
        exit($xml->asXML());
    }

    private function buildXml(\SimpleXMLElement $xml, array $data = [])
    {
        foreach ($data as $key => $valueData) {
            if ($key === 'xml') {
                continue;
            }

            $filteredKey = \XmlTools::filterGetSafeXmlNodeName($key);
            preg_match('/^([^ ]+) ?(.*)$/', $filteredKey, $nodeNames);

            if (!isset($nodeNames[1])) {
                throw new Exception(__CLASS__ . "::" . __METHOD__ . " - NameName missing from filter key: {$filteredKey}");
            }

            $nodeName = $nodeNames[1];

            if (is_object($valueData)) {
                $properties = ReflectionClass::getPublicProperties($valueData);

                $simpleXmlElement = $xml->addChild($nodeName);
                $this->addAttributes($simpleXmlElement, $nodeNames);

                $this->buildXml($simpleXmlElement, $properties);
            } else {
                if (is_array($valueData)) {
                    $simpleXmlElement = $xml->addChild($nodeName);
                    $this->addAttributes($simpleXmlElement, $nodeNames);

                    $this->buildXml($simpleXmlElement, $valueData);
                } else {
                    $simpleXmlElement = $xml->addChild($nodeName, $this->escape($valueData));
                    $this->addAttributes($simpleXmlElement, $nodeNames);
                }
            }
        }
        return $xml;
    }

    protected function addAttributes(\SimpleXmlElement $xml, $nodeNames)
    {
        if (isset($nodeNames[2])) {
            $attributes = array_filter(preg_split('/(.*[\"\']) /', $nodeNames[2], 0,
                PREG_SPLIT_DELIM_CAPTURE));

            foreach ($attributes as $keyValue) {
                $parts = explode('=', str_replace(['"', "'"], ['', ''], $keyValue));
                $xml->addAttribute($parts[0], $parts[1]);
            }
        }
    }

    protected function escape($string)
    {
        #return str_replace(array('\\'), array('\\\\'), $string);
        return $string;
    }
}
