<?php
/**
 * @author Chris Cunningham
 */

namespace FW\System\Actions\Abstracted;

use FW\System\Root\BaseClass;
use FW\System\Actions;
use FW\System\Utilities\Traits\MultiInheritance;

/**
 * @method  Actions\Collection add
 * @method  Actions\Collection insertBefore
 * @method  Actions\Collection insertAfter
 * @method  Actions\Collection execute
 */
abstract class Action extends BaseClass
{
    use MultiInheritance;

    public function __construct()
    {
        $this->inherit(new Actions\Collection);
    }

    /**
     * @var boolean
     */
    protected $isDisptched = false;

    abstract public function run();

    public function setDispatched()
    {
        $this->isDisptched = true;
    }

    public function isDispatched()
    {
        return $this->isDisptched;
    }

    /**
     * @return boolean
     */
    public function isActions()
    {
        /* @var $this Actions\Collection */
        return $this->count();
    }

}