<?php
/**
 * @author Chris Cunningham
 */

namespace FW\System\Extension;

class ReflectionClass extends \ReflectionClass
{
    public static function getPublicProperties($objectData)
    {
        if ($objectData instanceof \stdClass) {
            return \fwStdClassToArray($objectData);
        }
        $instance = new self($objectData);
        /* @var $instance \ReflectionClass */

        $return = [];
        /* @var $property \ReflectionProperty */
        foreach ($instance->getProperties(\ReflectionProperty::IS_PUBLIC) as $property) {
            $return[$property->getName()] = $property->getValue($objectData);
        }
        return $return;
    }

}