<?php

use FW\System\Error;

/**
 * Global Class
 * Class to hold global functions and generic tools for common tasks.
 *
 * @author chriscunningham
 */
class Tools
{
    /**
     * @param int $numberOfChars
     *
     * @return string
     */
    public static function randomString($numberOfChars = 8)
    {
        $randomPassword = '';
        for ($i = 0; $i < $numberOfChars; $i++) {
            if (!(rand(0, 3) % 3)) {
                $randomPassword .= chr(rand(65, 90));  // Chars A-Z
            } else {
                if (!(rand(0, 3) % 2)) {
                    $randomPassword .= chr(rand(97, 122)); // Chars a-z
                } else {
                    $randomPassword .= chr(rand(48, 57));  // Chars 0-9
                }
            }
        }
        return $randomPassword;
    }

    /**
     * Checks if a file exists using its namespace name instead of file path
     * Also handles case insensitive files systems and forces check to be
     * case sensitive.
     *
     * @param string $fileName
     * @param string $extension
     *
     * @return bool
     */
    public static function fileExists($fileName, $extension = '.php')
    {
        if (strpos($fileName, '\\') !== false) {
            $fileName = self::namespaceToDirectoryPath($fileName);
        }

        if ($fileName{0} != '/') {
            $fileName = "/{$fileName}";
        }

        $settings = \Settings::i();

        if (strpos($fileName, ($documentRoot = $settings->documentRoot())) === false) {
            $fullFilePath = $documentRoot . $fileName . $extension;
        } else {
            $fullFilePath = $fileName . $extension;
        }

        if ($settings->caseSensitiveFileSystem()) {
            return @file_exists($fullFilePath);
        }

        $filesParts = explode('/', $fileName);
        $fileShortName = end($filesParts);

        $files = glob(dirname($fullFilePath) . "/*{$extension}", GLOB_NOSORT);

        if (!$files) {
            return false;
        }

        $search = preg_quote($fileShortName . $extension);
        $matches = preg_grep("/\/{$search}$/",
            $files); // Use this instead of file_exists for case insensitive file systems like a Mac

        return count($matches) === 1; // If there is more than one result then we have a case issue of two files same name different case
    }

    /**
     * Parse's ini file into multidimensional array using ini format
     * [Level1.Level2.Level3.ect... : InheritFrom : ThenInheritFrom : ect.. ]
     *
     * @param string $filePath - e.g. /var/www/html/www.site.com/App/FW.ini
     * @param int    $returnType - FW_ARRAY | FW_OBJECT
     *
     * @return array
     */
    public static function parseIniFile($filePath, $returnType = FW_ARRAY)
    {
        if (!file_exists($filePath)) {
            trigger_error("Ini file does not exist {$filePath}.", E_USER_ERROR);
        }

        // Strip white space from beginning of each line
        $iniFileHandle = @fopen($filePath, 'r');
        $iniFile = '';
        while (($line = fgets($iniFileHandle, 4096)) !== false) {
            $iniFile .= ltrim($line);
        }
        fclose($iniFileHandle);

        $settings = parse_ini_string($iniFile, true); // Use standard php ini parser for normal ini formatting

        // Extend parsing with additional formatting
        $debug = $temp = [];
        foreach ($settings as $longKey => &$valueData) {
            // Process multi inheritance
            $inheritKeys = explode(':', $longKey);
            $childKeys = array_shift($inheritKeys);
            if (count($inheritKeys)) {
                foreach ($inheritKeys as $inheritFrom) {
                    $inheritFrom = trim($inheritFrom);
                    $inheritData = array_intersect_key($settings, array_flip(array($inheritFrom)));
                    if (!$inheritData) {
                        $debug[] = "Can not inherit from: {$inheritFrom} please check your config in {$filePath}";
                        continue;
                    }
                    $valueData = array_merge($inheritData[key($inheritData)], $settings[$longKey]);
                }
            }

            // Process nested object property syntax and merge on top of inherited data
            if (is_array($valueData)) {
                foreach ($valueData as $key => $valData) {
                    if (strpos($key, '.') !== false) {
                        $bottomLevel = explode('.', $key);
                        unset($valueData[$key]);
                        $valueData = self::arrayMergeArrayValuesAsKeys($bottomLevel, $valData, $valueData);
                    }
                }
            }

            // Merge reformatted data into new array
            $keys = explode('.', $childKeys);
            foreach ($keys as &$value) {
                $value = trim($value);
            }
            $temp = self::arrayMergeArrayValuesAsKeys($keys, $valueData, $temp);
        }

        // Optional return parsed data as stdObjects
        if ($returnType == FW_OBJECT) {
            $temp = fwArrayToStdClass($temp);
        }

        // Handle any parse errors
        if ($debug) {
            foreach ($debug as $message) {
                @error_log($message);
            }
            trigger_error('Your ini file has errors check your error logs.', E_USER_ERROR);
        }

        return $temp;
    }

    /**
     * Still in development
     *
     * @param string $filePath
     * @param int    $returnType
     *
     * @throws Error\Exception
     */
    public static function parseYamlFile($filePath, $returnType = FW_ARRAY)
    {
        $debug = $temp = [];
        $file = '';
        $lineCount = 0;

        $fileHandle = fopen($filePath, 'r');
        $iniFile = '';

        while (($line = fgets($fileHandle, 4096)) !== false) {
            $lineCount++;
            if (strpos($line, ';') === 0) {
                continue;
            } // If line starts with a comment char then skip the whole line

            $resultLine = preg_replace('/;(.*)/', '', $line);
            if (ctype_space($resultLine)) {
                continue;
            } // Skip white space lines

            preg_match('/(?=\s*)([^:\s]+):\s*(.*)/', $resultLine, $matches);
            unset($matches[0]);

            // TODO:: Complete this function it is not really needed now so moving on
            if (!isset($matches[1])) {
                $debug[] = "FW_ERROR: {$filePath} : Error line: {$lineCount} no key seperator ':'";
                continue;
            }

            $file .= $resultLine;
        }
        fclose($fileHandle);

        if ($returnType) {
            echo $file;
        }

        if ($debug) {
            foreach ($debug as $message) {
                @error_log($message);
            }
            throw new Error\Exception('Your yaml file has errors check your error logs.');
        }
        exit();
    }

    /**
     * @example
     * $keys = ['level1', 'level2', 'level3', 'new_key']
     * $valueData = 'ValueToSet'
     * $subjectData =
     * array(
     *     'level1' => array(
     *         'level2' => array(
     *             'a_level2_key' = 'some_value2',
     *             'level3' => array(
     *                 'a_level3_key' => 'some_value3'
     *             )
     *         )
     *     )
     * );
     * @return array -
     * array(
     *     'level1' => array(
     *         'level2' => array(
     *             'a_level2_key' = 'some_value2',
     *             'level3' => array(
     *                 'a_level3_key' => 'some_value3'
     *                 'new_key' => 'ValueToSet'
     *             )
     *         )
     *     )
     * );
     */
    /**
     * @param array $keys
     * @param mixed $valueData
     * @param array $subjectData
     *
     * @return array - array2, array3 - $array2, $array3 etc... additional arrays passed will also be merged in
     */
    public static function arrayMergeArrayValuesAsKeys(array $keys, $valueData, array $subjectData)
    {
        $funcArgs = func_get_args();
        $funcArgs = array_slice($funcArgs, 3);

        $lastLevel = json_encode($valueData);
        $jsonData = json_decode('{"' . implode('":{"', $keys) . '":' . $lastLevel . str_repeat('}',
                count($keys)), true);

        $callData[] = $subjectData;
        $callData[] = $jsonData;
        $callData = array_merge($callData, $funcArgs);

        return call_user_func_array('array_merge_recursive', $callData);
    }

    /**
     * Checks if two strings are anagrams of each other
     *
     * @param string $subject - subject string.
     * @param string $compare - string to check. Find out if this is an anagram of the subject string.
     *
     * @return boolean
     */
    public static function isAnagram($subject, $compare)
    {
        // assuming we don't care about letter case or spaces
        $subject = strtolower(str_replace(' ', '', $subject));
        $compare = strtolower(str_replace(' ', '', $compare));

        // if the number of chars do not match then it can not contain the same letters
        if (strlen($subject) != strlen($compare)) {
            return false;
        }

        // split the strings into arrays for array_diff comparison
        return !array_diff(str_split($subject), str_split($compare));
    }

    /**
     * Output headers for xml
     *
     * @param string $type
     */
    public static function setContentTypeHeader($type = null)
    {
        switch ($type) {
            case 'xml':
                $contentType = 'text/xml';
                break;
            case 'json':
                $contentType = 'application/json';
                break;
            case 'html':
            default:
                $contentType = 'text/html; charset=UTF-8';
        }
        header("Content-type: $contentType");
    }

    /**
     * Server Compatibility tools
     */

    const SERVER_OS_LINUX = 'Linux';
    const SERVER_OS_MAC = 'Darwin';
    const SERVER_OS_WINDOWS = 'WINNT';

    public static function detectServerOs($oS = self::SERVER_OS_MAC)
    {
        return PHP_OS == $oS;
    }

    public static function namespaceToDirectoryPath($nameSpace)
    {
        return str_replace('\\', '/', $nameSpace);
    }

    public static function directoryPathToNamespace($path)
    {
        return str_replace('/', '\\', $path);
    }

    public static function arrayToDirectoryPath(array $contextArray)
    {
        return implode('/', $contextArray);
    }

    public static function arrayToNameSpace(array $contextArray)
    {
        return implode('\\', $contextArray);
    }

    public static function dirLevelUp($multiplier)
    {
        return str_repeat('/' . '..', $multiplier) . '/';
    }

    /**
     * Get the number of bits that the
     * system uses for an integer
     * Or check if system is equal to $bits
     * @example 32 bit system will return 32
     * @example 64 bit system will return 64
     * @example 128 bit system will return 128
     * @example 48 bit system will return 48 - if such a server exists for PHP
     *
     * @param null $bits
     *
     * @return bool|int
     */
    public static function systemBits($bits = null)
    {
        // Switch every bit for an integer using the tilde() operator~
        // and count how may bits there are
        $systemBitsInInteger = strlen(decbin(~0));
        return is_null($bits) ? $systemBitsInInteger : $systemBitsInInteger == $bits;
    }

    /**
     * get the number of bits in one system byte
     * Would never really need to use this as
     * normal systems are built with 8 bits in 1 byte
     * but just wrote this because it is possible to do so.
     * @return int
     */
    public static function bitsInByte()
    {
        return self::systemBits() / PHP_INT_SIZE;
    }

    /**
     * @param $url
     *
     * @return int - number of bytes
     */
    public static function getSizeOfExternalFile($url)
    {
        $url = escapeshellarg($url);

        $contentLength = exec("wget {$url} --spider --server-response -O - 2>&1 | grep Content-Length");

        return preg_replace('/[^:]+:[^0-9]*/', '', $contentLength);
    }

    public static function fwFileExistsRemote($path)
    {
        return (@fopen($path, "r") == true);
    }

    public static function createDirectory($directory, $permissions = 0775)
    {
        if(!is_dir($directory) && !file_exists($directory)) {
            $oldUMask = umask(0);
            if(!mkdir($directory, $permissions, true)) {
                umask($oldUMask); // Restore umask
                throw new \Exception(__CLASS__."::".__FUNCTION__." - Unable to create directory {$directory}");
            }
            umask($oldUMask);
        }
    }

}