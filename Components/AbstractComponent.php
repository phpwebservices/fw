<?php
/**
 * @author Chris Cunningham
 */

namespace FW\Components;

use FW\System\Root;
use FW\System\Error;

abstract class AbstractComponent extends Root\BaseComponent
{
    /**
     * @var \FW\Components\AbstractCollection
     * @aggregate
     */
    protected $componentsCollection = null;

    /**
     * @param array|\FW\Components\AbstractCollection $componentsCollectionData
     *
     * @throws Error\Exception
     */
    public function __construct($componentsCollectionData = null)
    {
        $this->componentsCollection = new AbstractCollection;

        if (!$componentsCollectionData) {
            return;
        }

        if (is_array($componentsCollectionData)) {
            foreach ($componentsCollectionData as $index => $abstractComponent) {
                if (!$abstractComponent instanceof Root\BaseComponent) {
                    throw new Error\Exception("Must be an array of \\FW\\System\\Root\\BaseComponent objects instead the following was passed: " . print_r($componentsCollectionData,
                            true));
                }
                $this->componentsCollection->add($abstractComponent);
            }
            return;
        }

        if (!$componentsCollectionData instanceof AbstractCollection) {
            throw new Error\Exception(get_class($this) . "Must be a collection of type \\FW\\Components\\Collection");
        }

        $this->componentsCollection = $componentsCollectionData;
    }

    /**
     * @return boolean
     */
    public function hasComponentsCollection()
    {
        return $this->componentsCollection instanceof AbstractCollection && $this->componentsCollection->count();
    }

    /**
     * @return \FW\Components\AbstractCollection
     */
    public function getComponentsCollection()
    {
        return $this->componentsCollection;
    }

    /**
     * @param Root\BaseComponent|AbstractComponent $abstractComponent
     *
     * @return mixed|\FW\Components\AbstractCollection
     */
    public function add(Root\BaseComponent $abstractComponent)
    {
        return $this->componentsCollection->add($abstractComponent);
    }

    const REMOVE_AND_REINDEX = true;

    /**
     * @param string $componentClassName
     * @param bool   $removeAndReIndex
     *
     * @return mixed|void|\FW\Components\AbstractComponent
     */
    public function component($componentClassName, $removeAndReIndex = false)
    {
        if (!$this->componentsCollection->count()) {
            return false;
        }
        return $this->componentsCollection->component($componentClassName, $removeAndReIndex);
    }

    /**
     * @throws Error\Exception
     * @return void|string
     */
    public function getComponentsOutput()
    {
        if (!$this->componentsCollection->count()) {
            return false;
        }
        return $this->componentsCollection->getOutput($this->data);
    }

}