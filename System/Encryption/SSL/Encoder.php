<?php
/**
 * @author Chris Cunningham
 */

/**
 * Class to handle encrypting and decrypting data using SSL for the purpose
 * of requests being made over mobile networks as some network providers
 * intercept requests before passing the data on to a secure SSL server.
 * PHP raw post data from file_get_contents('php://input') or $_GET data.
 * Uses 1024 bit to 4092 bit SSL public private keys
 *
 * @author Chris Cunningham
 */

//namespace FW\System\Encryption\SSL;
namespace Encryption\SSL;

use FW\System\Error\Exception;

final class Encoder
{
    const SSL_MIN_PKCS1_PADDING = 11;

    const SSL_PUBLIC_KEY = 'public.pem';
    const SSL_PRIVATE_KEY = 'private.pem';

    public static function encryptRawPostData($dataData)
    {
        return self::encryptData(http_build_query($dataData));
    }

    public static function decryptRawPostData($encodedData)
    {
        parse_str(self::decryptData($encodedData), $returnParams);
        return $returnParams;
    }

    public static function encryptGetRequest(array $parameters)
    {
        return urlencode(self::encryptData(serialize($parameters)));
    }

    public static function decryptGetRequest($encodedData)
    {
        return unserialize(self::decryptData(urldecode($encodedData)));
    }

    public static function encryptData($data)
    {
        $encodedData = '';

        $publicKeyHandle = self::getPublicKey();

        $maxBlockSize = self::getMaxBlockSize($publicKeyHandle);

        while ($data) {
            $input = substr($data, 0, $maxBlockSize - self::SSL_MIN_PKCS1_PADDING);
            $data = substr($data, $maxBlockSize - self::SSL_MIN_PKCS1_PADDING);

            if (@openssl_public_encrypt($input, $cryptedData, $publicKeyHandle, OPENSSL_PKCS1_PADDING)) {
                $encodedData .= $cryptedData;
            } else {
                openssl_free_key($publicKeyHandle);
                throw new Exception('EncryptionSSL::crypt::openssl_public_encrypt: Level 1 Public key could not encrypt: ' . self::getError());
            }
        }

        openssl_free_key($publicKeyHandle);

        return base64_encode($encodedData);
    }

    public static function decryptData($encodedData)
    {
        $encodedData = base64_decode($encodedData);

        $decodedData = '';

        $privateKeyHandle = self::getPrivateKey();

        $maxBlockSize = self::getMaxBlockSize($privateKeyHandle);

        while ($encodedData) {
            $input = substr($encodedData, 0, $maxBlockSize);
            $encodedData = substr($encodedData, $maxBlockSize);

            if (@openssl_private_decrypt($input, $decryptedData, $privateKeyHandle, OPENSSL_PKCS1_PADDING)) {
                $decodedData .= $decryptedData;
            } else {
                openssl_free_key($privateKeyHandle);
                throw new Exception('EncryptionSSL::decrypt::openssl_private_decrypt: Could not decrypt: ' . self::getError());
            }
        }

        openssl_free_key($privateKeyHandle);

        return $decodedData;
    }

    protected static function getPublicKey()
    {
        if (!file_exists(($publicFile = __DIR__ . DIRECTORY_SEPARATOR . self::SSL_PUBLIC_KEY))) {
            throw new Exception("EncryptionSSL::crypt: Failed to find public key: {$publicFile}",
                Exception::E_PROGRAMMER_NOTICE);
        }

        $publicKeyHandle = openssl_pkey_get_public(file_get_contents($publicFile));

        if (!is_resource($publicKeyHandle)) {
            throw new Exception('EncryptionSSL::crypt: Failed to open public key: ' . self::getError(),
                Exception::E_PROGRAMMER_NOTICE);
        }
        return $publicKeyHandle;
    }

    protected static function getPrivateKey()
    {
        if (!file_exists(($privateFile = __DIR__ . DIRECTORY_SEPARATOR . self::SSL_PRIVATE_KEY))) {
            throw new Exception("EncryptionSSL::crypt: Failed to find public key: {$privateFile}",
                Exception::E_PROGRAMMER_NOTICE);
        }

        $privateKeyHandle = openssl_pkey_get_private(file_get_contents($privateFile));

        if (!is_resource($privateKeyHandle)) {
            throw new Exception('EncryptionSSL::decrypt: Failed to open private key: ' . self::getError());
        }
        return $privateKeyHandle;
    }

    protected static function getMaxBlockSize($keyHandle)
    {
        $certInfo = openssl_pkey_get_details($keyHandle);
        return $certInfo['bits'] / 8; // Return number of bytes per block

    }

    protected static function getError()
    {
        while ($messages[] = openssl_error_string()) {}

        $messages = array_filter($messages);

        foreach ($messages as &$value) {
            $value = "<error>$value</error>";
        }
        return implode(PHP_EOL, $messages);
    }

}