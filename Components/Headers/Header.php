<?php
/**
 * @author Chris Cunningham
 */

namespace FW\Components\Headers;

use FW\Components\AbstractComponent;

class Header extends AbstractComponent
{
    public function output()
    { ?>
        <div class="header">
        <?php echo $this->getComponentsOutput(); ?>
        </div><?php
    }

}