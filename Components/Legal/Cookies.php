<?php
/**
 * Created by PhpStorm.
 * User: chriscunningham
 * Date: 04/01/2014
 * Time: 18:25
 */

namespace FW\Components\Legal;

use FW\System\Root;
use FW\Components\AbstractComponent;

class Cookies extends AbstractComponent
{
    protected $hideCookie = null;

    public function __construct($componentsCollectionData = null)
    {
        $this->hideCookie = \Request::i()->get('hide-cookie');

        if ($this->hideCookie) {
            setcookie('CookiesBanner', 1, strtotime('+100 year'));
        } else {
            if (!isset($_COOKIE['CookiesBanner'])) {
                setcookie('CookiesBanner', 0, strtotime('+100 year'));
            }
        }

        parent::__construct($componentsCollectionData);
    }

    public function output()
    {
        if ($this->hideCookie !== '1' && (!isset($_COOKIE['CookiesBanner']) || !$_COOKIE['CookiesBanner'])) { ?>
            <div class="cookie">
                <p>This site has placed some cookies on your computer to help make this website better. You can <a href="http://www.aboutcookies.org" target="_blank">change your cookie settings</a> at any time. Otherwise, we'll assume you're OK to continue.</p>
                <a class="close-button" title="Stops this cookie message from showing again" href="<?= \Context::currentUrl(['hide-cookie' => 1]) ?>">Close Message</a>
            </div>
        <?php }
    }


}