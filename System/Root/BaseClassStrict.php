<?php
/**
 * @author Chris Cunningham
 */

namespace FW\System\Root;

use FW\System\Interfaces;
use FW\System\Error;

class BaseClassStrict
{
    /**
     * @var array
     */
    private $data = [];

    /**
     * Provide a means for your inheriting class to disable or enable
     * the ability to set public data allowing for read only access to data.
     * @var boolean
     */
    private $enableWriteAccess = false;

    /**
     * Provide a means for your inheriting class to disable or enable
     * the ability to write public data via public property syntax.
     * @example - When this is set to false this following will error $instance->publicProperty = 1;
     * @var boolean
     */
    private $enablePublicPropertyWriteAccess = false;

    /**
     * enable/disable write access to object data.
     *
     * @param boolean $value
     *
     * @return bool
     */
    public function writeAccess($value = null)
    {
        if (is_null($value)) {
            return $this->enableWriteAccess;
        }
        $this->enableWriteAccess = $value;
    }

    /**
     * enable/disable write access to object data.
     *
     * @param boolean $value
     *
     * @return bool
     */
    public function publicPropertyWriteAccess($value = null)
    {
        if (!is_null($value)) {
            $this->enablePublicPropertyWriteAccess = $value;
        }
        return $this->enableWriteAccess && $this->enablePublicPropertyWriteAccess;
    }

    /**
     * @param array $data
     */
    protected function __construct(array $data = [])
    {
        $this->data($data);
    }

    /**
     * @param array $data
     *
     * (non-PHPdoc)
     * @see \FW\System\Interfaces\DataBean::data()
     *
     * @return array
     */
    protected function data(array $data = [])
    {
        if (!$data) {
            foreach ($this->data as $methodName => $valueData) {
                $data[$methodName] = $this->{$methodName}();
            }
            return $data;
        }
        foreach ($data as $methodName => $valueData) {
            $this->{$methodName}($valueData);
        }
        return $this->data;
    }

    protected function setData($data)
    {
        // TODO Only allow call from cache setter calls
        $this->data = $data;
    }

    protected function getData()
    {
        // TODO Only allow call from cache setter calls
        return $this->data;
    }

    /**
     * (non-PHPdoc)
     * @see FW\System\Interfaces.DataBean::resetData()
     */
    public function resetData()
    {
        $this->data = [];
    }

    public function __set($methodName, $valueData)
    {
        if (!$this->publicPropertyWriteAccess()) {
            throw new Error\Exception("Error " . get_class($this) . "->{$methodName} - Setting data via public property access has been disabled for this object");
        }
        if (!method_exists($this, $methodName)) {
            throw new Error\Exception("Error " . get_class($this) . "->{$methodName} - You are trying to set a property with out a explicit unified setter function");
        }
        return $this->{$methodName}($valueData);
    }

    public function __get($methodName)
    {
        if (!method_exists($this, $methodName)) {
            throw new Error\Exception("Error " . get_class($this) . "->{$methodName} - You are trying to get a property with out a explicit unified getter function");
        }
        return $this->{$methodName}();
    }

    protected function access($methodName, $valueData = null, $defaultData = null)
    {
        if (is_null($valueData)) {
            return isset($this->data[$methodName]) ? $this->data[$methodName] : $defaultData;
        }
        if (!$this->writeAccess()) {
            throw new Error\Exception("Error " . get_class($this) . "->{$methodName} - Setting data has been disabled for this object");
        }
        return ($this->data[$methodName] = $valueData);
    }

    public function __isset($methodName)
    {
        return isset($this->data[$methodName]);
    }

    public function __unset($methodName)
    {
        unset($this->data[$methodName]);
    }

    /**
     * Triggered when invoking inaccessible methods in an object context.
     *
     * @param $methodName
     * @param $arguments
     *
     * @throws \Exception
     */
    public function __call($methodName, $arguments)
    {
        throw new \Exception(get_called_class() . " - " . __METHOD__ . "() - Method: <b>{$methodName}</b> does not exist.");
    }

    protected function getErrorLocation($callingFunction)
    {
        return get_class($this) . "->{$callingFunction}() - " . __FILE__;
    }
}