<?php
/**
 * @author Chris Cunningham
 */
/**
 * Global functions that have been tested to be fasted than standard php functions
 */

/**
 * @performance - 98.52% faster than array_shift
 * on large data arrays over 2000 records. Due to
 * native function re indexing the array each time
 *
 * @param array $data
 *
 * @return mixed
 */
function fwArrayShift(array &$data = array())
{
    if (!$data) {
        return false;
    }
    reset($data);
    $returnData = current($data);
    unset($data[key($data)]);
    return $returnData;
}

/**
 * @return string
 */
function fwGetScheme()
{
    if (isset($_SERVER['HTTP_X_FORWARDED_PROTO'])) {
        return $_SERVER['HTTP_X_FORWARDED_PROTO'];
    }
    return isset($_SERVER['HTTPS']) ? "https" : "http";
}

/**
 * @return string
 */
function fwGetHost()
{
    return $_SERVER['HTTP_HOST'];
}

/**
 * @return string
 */
function fwGetDomain()
{
    return \fwGetScheme() . '://' . \fwGetHost();
}

/**
 * Temporary function until PHP 7 is released.
 *
 * Note!!! Change all places where this function is used to use the new PHP 7 operator "??" called "Null Coalesce Operator"
 *
 * @see https://wiki.php.net/rfc/isset_ternary
 *
 * @param      $key
 * @param      $dataData
 * @param null $defaultData
 *
 * @return null
 */
function G($key, $dataData, $defaultData = null)
{
    if (is_array($dataData)) {
        return isset($dataData[$key]) ? $dataData[$key] : $defaultData;
    } else {
        if (is_object($dataData)) {
            return isset($dataData->$key) ? $dataData->$key : $defaultData;
        } else {
            return $defaultData;
        }
    }
}

/**
 * @param null $value1Data
 * @param null $value2Data
 *
 * @return null
 */
function I($value1Data = null, $value2Data = null)
{
    return $value1Data ? $value1Data : $value2Data;
}

/**
 * @param      $dataData
 * @param bool $returnName
 *
 * @return int|string
 */
function fwGetType($dataData, $returnName = false)
{
    switch (true) {
        case is_bool($dataData):
            return $returnName ? 'boolean' : FW_BOOL;
            break;
        case is_string($dataData):
            return $returnName ? 'string' : FW_STRING;
            break;
        case is_array($dataData):
            return $returnName ? 'array' : FW_ARRAY;
            break;
        case is_object($dataData):
            return $returnName ? 'object' : FW_OBJECT;
            break;
        case is_resource($dataData):
            return $returnName ? 'resource' : FW_RESOURCE;
            break;
        case is_null($dataData):
            return $returnName ? 'null' : FW_NULL;
            break;
        case is_numeric($dataData):
            return $returnName ? 'numeric' : FW_NUMERIC;
            break;
        default:
            return $returnName ? 'unknown' : FW_UNKNOWN;
    }
}

/**
 * Split a string by capital letters
 * @example camelCaseString becomes ['camel', 'Case', 'String']
 *
 * @param string $string
 *
 * @return array
 */
function fwUcSplit($string)
{
    return array_filter(preg_split('/(?=[A-Z0-9])/', $string));
}

/**
 * @param string $url
 *
 * @return mixed
 */
function fwUrlEncode($url)
{
    return str_ireplace(['%2F', '%20', '%3A'], ['/', '+', ':'], urlencode(urldecode($url)));
}

/**
 * @param stdClass $stdClass
 *
 * @return array
 */
function fwStdClassToArray(stdClass $stdClass)
{
    $data = (array)$stdClass;
    foreach ($data as &$dataData) {
        if ($dataData instanceof stdClass) {
            $dataData = fwStdClassToArray($dataData);
        }
    }
    return $data;
}

/**
 * @param array $data
 *
 * @return StdClass
 */
function fwArrayToStdClass(array $data)
{
    $data = (object)$data;
    foreach ($data as &$dataData) {
        if (is_array($dataData)) {
            $dataData = fwArrayToStdClass($dataData);
        }
    }
    return $data;
}

function fwCachePrefix($cacheKey)
{ // TODO: This isn't very secure host name can be spoofed
    $domain = sha1(fwGetHost());
    return $domain . '_' . $cacheKey;
}

function fwGetCalledFunction()
{
    return @debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2)[1]['function'];
}

function fwGetCaller($i = 2)
{
    $a = @debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, $i)[$i - 1];
    if (!$a) {
        return false;
    }
    return @"{$a['class']}{$a['type']}{$a['function']}";
}

function fwGetFileLine($i = 2)
{
    $a = @debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, $i)[$i - 1];
    if (!$a) {
        return false;
    }
    return @"{$a['file']}:{$a['line']}";
}

function fwLog($data, $i = 2, $fileName = 'notice-log')
{
    static $logFilePath = '';

    $location = date('Y-m-d H:i:s') . ": " . fwGetCaller($i) . " - " . fwGetFileLine($i) . " - ";

    if (!$logFilePath) {
        $settings = \Settings::i();
        $logFilePath = pathinfo($settings->errorLogPath(), PATHINFO_DIRNAME) . "/{$settings->domain()}-{$fileName}";
    }

    $changePermissions = false;
    if (!file_exists($logFilePath)) {
        $changePermissions = true;
    }

    $return = file_put_contents($logFilePath, $location . print_r($data, true) . PHP_EOL, FILE_APPEND);
    if ($changePermissions) {
        chmod($logFilePath, 0640);
    }

    return $return;
}

function fwExecInBackground($command, &$output)
{
    exec("{$command} > /dev/null 2>&1 &", $output);
    return $output;
}

function fwSortArrayByArrayKeys($inData, $sortByKeys, $fillMissing = false)
{
    $sortedData = array();
    foreach($sortByKeys as $key => $value) {
        if(isset($inData[$key])) {
            $sortedData[$key] = $inData[$key];
        } else if($fillMissing) {
            $sortedData[$key] = null;
        }
    }
    return $sortedData;
}

function fwTrimDuplicates($string, $char = ' ') {
    $char = preg_quote($char);
    return preg_replace("/[{$char}]{2,}/", $char, $string);
}
