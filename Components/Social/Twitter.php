<?php
/**
 * Created by PhpStorm.
 * User: chriscunningham
 * Date: 28/12/2013
 * Time: 19:11
 */

namespace FW\Components\Social;

use FW\System\Root;
use FW\Components\AbstractComponent;

class Twitter extends AbstractComponent
{
    protected $domain = '';
    protected $userName = '';

    public function setDomain($domain)
    {
        $this->domain = $domain;
    }

    public function setUserName($userName)
    {
        $this->userName = $userName;
    }

    public function output()
    {
    }

    public function tweetButton($domain = '', $userName = '')
    {
        $domain = 'data-url="' . ($domain ?: $this->domain) . '"';
        $userName = $userName ?: $this->userName;

        return <<<HTML
            <a href="https://twitter.com/share" class="twitter-share-button" {$domain} data-via="{$userName}" data-size="medium">Tweet</a>
HTML;
    }

    public function js()
    {
        return <<<HTML
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if (!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
HTML;
    }

    public function followButton($userName = '')
    {
        $userName = $userName ?: $this->userName;
        return <<<HTML
            <a href="https://twitter.com/{$userName}" class="twitter-follow-button" data-show-count="true" data-lang="en">Follow @{$userName}</a>
HTML;

    }

}