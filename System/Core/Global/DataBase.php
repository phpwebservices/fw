<?php
/**
 * @author Chris Cunningham
 */

// Global Class

use FW\System\Interfaces;

use FW\System\Root;
use FW\System\Error;
use FW\System\Error\Exception;

class DataBaseException extends Error\Exception
{
}

class DataBase extends \PDO implements Interfaces\ModuleSettings
{
    const MAIN_DB = 1;

    /**
     * @var array
     */
    private static $instances = [];

    /**
     * @var null|integer
     */
    private static $dataBaseInstanceId = null;

    public static function useDataBase($dataBaseInstanceId = self::MAIN_DB)
    {
        return self::$dataBaseInstanceId = $dataBaseInstanceId;
    }

    public static function initiate(\stdClass $moduleSettings)
    {
        self::validateSettings($moduleSettings);

        if (!$moduleSettings->isEnabled) {
            return;
        }

        \DataBase::useDataBase(\DataBase::MAIN_DB);
        \DataBase::addConnection(
            $moduleSettings->MySql->host,
            $moduleSettings->MySql->port,
            $moduleSettings->MySql->databaseName,
            $moduleSettings->MySql->user,
            $moduleSettings->MySql->password,
            \DataBase::MAIN_DB);

        if (G('Sessions', $moduleSettings)) {
            \Session::setUseDataBaseSession(true);
        }
    }

    /**
     * @param     $mySqlHost
     * @param     $port
     * @param     $dateBaseName
     * @param     $mysqlUser
     * @param     $mysqlPassword
     * @param int $dataBaseInstanceId
     *
     * @return DataBase
     */
    public static function addConnection($mySqlHost, $port, $dateBaseName, $mysqlUser, $mysqlPassword, $dataBaseInstanceId = self::MAIN_DB)
    {
        $db = self::$instances[$dataBaseInstanceId] = new self(
            "mysql:host={$mySqlHost};port={$port};dbname={$dateBaseName}", $mysqlUser, $mysqlPassword);

        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        return $db;
    }

    /**
     * @param null $dataBaseInstanceId
     *
     * @return DataBase
     */
    public static function i($dataBaseInstanceId = null)
    {
        return self::$instances[$dataBaseInstanceId ? $dataBaseInstanceId : self::$dataBaseInstanceId];
    }

    /**
     * @param stdClass $moduleSettings
     *
     * @throws FW\System\Error\Exception
     */
    private static function validateSettings(\stdClass $moduleSettings)
    {
        if (!($mySql = G('MySql', $moduleSettings))) {
            throw new Exception("Missing MySql : " . print_r($mySql, 1));
        }
        if (!($host = G('host', $mySql)) || !is_string($host)) {
            throw new Exception("Missing MySql->host : " . print_r($host, 1));
        }
        if (!($port = G('port', $mySql)) || !is_numeric($port)) {
            throw new Exception("Missing MySql->port : " . print_r($port, 1));
        }
        if (!($databaseName = G('databaseName', $mySql)) || !is_string($databaseName)) {
            throw new Exception("Missing MySql->databaseName : " . print_r($databaseName, 1));
        }
        if (!($user = G('user', $mySql)) || !is_string($user)) {
            throw new Exception("Missing MySql->user : " . print_r($user, 1));
        }
        if (!($password = G('password', $mySql)) || !is_string($password)) {
            throw new Exception("Missing MySql->password : " . print_r($password, 1));
        }
    }
}