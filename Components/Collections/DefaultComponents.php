<?php
/**
 * @author Chris Cunningham
 */

namespace FW\Components\Collections;

use FW\Components\Html;
use FW\Components\Head;
use FW\Components\Social\FaceBook;
use FW\Components\Headers\Header;
use FW\Components\Logo;
use FW\Components\User\Login;
use FW\Components\Navigations\Navigation;
use FW\Components\Navigations\NavigationPane;
use FW\Components\Content;
use FW\Components\Footers\Footer;

use FW\Components\AbstractCollection;
use FW\Components;

use FW\System\Actions;

use FW\System\Error;

class DefaultComponents extends AbstractCollection
{
    public function __construct()
    {
        $context = \Context::i();

        $default = new AbstractCollection;

        $default->add((new Head)
            ->title('Default Title')
            ->addUTF8()
            ->addLastModified('17th Nov 2012 13:34')
            ->addBaseUrl($context->baseUrl())
            ->addKeywords()
            ->addDescription()
            ->addAuthor()
            ->addRobots()
            ->addCopyright()
            ->addCanonical($context->stringAbsoluteLoadedController())#->addCanonical($context->stringAbsolute())
            ->addGoogleAnalytics(\Settings::i()->googleAnalytics()));

        $default
            ->add(new FaceBook)
            ->add(new Header([new Logo, new Navigation, new Login]))
            ->add(new NavigationPane)
            ->add(new Content)
            ->add(new Footer);

        $this->add(new Html($default));
    }

    public static function actions()
    {
        return (new Actions\Collection)->add(Login::actions());
    }

    /** @return Html */
    public function Html()
    {
        return $this->component('Html');
    }

    /** @return Head */
    public function Head()
    {
        return $this->component('Head');
    }

    /** @return Login */
    public function Login()
    {
        return $this->component('Login');
    }

    /** @return Navigation */
    public function Navigation()
    {
        return $this->component('Navigation');
    }

    /** @return Footer */
    public function Footer()
    {
        return $this->component('Footer');
    }

    /**
     * @param Components\AbstractComponent $viewComponent
     * @param string $insertIntoComponentSpaceName
     *
     * @throws Error\Exception
     * @return Content
     */
    public function Content(
        Components\AbstractComponent $viewComponent = null,
        $insertIntoComponentSpaceName = '\App\Components\Content'
    ) {
        if (is_null($viewComponent) && ($content = $this->component('Content')) instanceof Content) {
            return $content;
            /* @var $content Content */
        }
        if (!$this->insertInto($viewComponent, $insertIntoComponentSpaceName)) {
            throw new Error\Exception(get_called_class() . " - " . __FUNCTION__ . " $insertIntoComponentSpaceName Does not exist failed to insert " . get_class($viewComponent));
        }
    }

}