<?php
/**
 * Created by PhpStorm.
 * User: chriscunningham
 * Date: 15/12/2013
 * Time: 23:49
 */

namespace FW\Components\Media\Video;

class Youtube extends Flash
{
    protected $baseUrl = 'https://www.youtube.com/v/%s?version=3';

    public function __construct($url, $width = null, $height = null)
    {
        parent::__construct(sprintf($this->baseUrl, $url), $width, $height);
    }
}