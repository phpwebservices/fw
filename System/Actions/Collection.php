<?php
/**
 * @author Chris Cunningham
 */

namespace FW\System\Actions;

use FW\System\Error\Exception;

use FW\System\Actions\Abstracted\Action;
use FW\System\Root;
use FW\System\Error;
use FW\System\Utilities\Traits\Singleton;
use FW\System\Utilities\Traits\MultiInheritance;

/**
 * @method \FW\System\Actions\Collection i()
 */
class Collection extends Root\ObjectCollection
{
    /**
     * @var Collection
     * @return Collection
     */
    use Singleton, MultiInheritance;

    /**
     * @param Action $action
     *
     * @throws Exception
     * @return \FW\System\Actions\Collection
     */
    public function add(Action $action)
    {
        if ($this->inCollection($action) && !$action->isDispatched()) {
            throw new Error\Exception(get_class($action) . ": Action already exists in
                    stack and not executed. infinate loop detected execution halted. StackData: " . print_r($this->data,
                    true));
        }
        parent::addObject($action);
        return $this;
    }

    /**
     * @param string     $actionName
     * @param Controller $newAction
     *
     * @return \FW\System\Actions\Collection
     */
    public function insertBefore($actionName, Action $newAction)
    {
        return parent::insertObjectBefore($actionName, $newAction);
    }

    /**
     * @param string     $actionName
     * @param Controller $newAction
     *
     * @return \FW\System\Actions\Collection
     */
    public function insertAfter($actionName, Action $newAction)
    {
        return parent::insertObjectAfter($actionName, $newAction);
    }

    /**
     * @return void
     */
    public function execute()
    {
        // Use for loop as Action can be added whilst looping. foreach's count remains the same as when the loop was first started
        for ($i = 0; $i < $this->count(); $i++) {
            $action =& $this->iteratorData[$i];
            /* @var $action \FW\System\Actions\Abstracted\Action */
            if (!$action->isDispatched()) {
                $action->run();
                if ($action->isActions()) {
                    $action->execute();
                }
                $action->setDispatched();
            }
        }
    }

}