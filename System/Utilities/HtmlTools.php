<?php
/**
 * Created by PhpStorm.
 * User: chriscunningham
 * Date: 22/02/2014
 * Time: 00:41
 */

use FW\System\Error;

class HtmlTools
{
    /**
     * Clean up broken html and output it
     *
     * @param $hTML
     *
     * @return int
     * @throws FW\System\Error\Exception
     */
    public static function printTidyHtml($hTML)
    {
        $options = [
            'wrap' => 512,
            'indent' => true,
            'indent-spaces' => 4,
            'sort-attributes' => 'alpha',
            'output-html' => true,
            'output-xhtml' => true,
            'preserve-entities' => true,
            'quote-ampersand' => true,
            'quote-marks' => true,
            'merge-divs' => false
        ];

        $tidy = new \tidy();

        # This can repair bad html entities... think it is better
        # to let it error and fix the entities from the source error location
        #
        # $clean = $tidy->repairString($hTML);
        # $tidy->parseString($clean, $options, 'utf8');

        $tidy->parseString($hTML, $options, 'utf8');

        $tidy->cleanRepair();

        if ($tidy->errorBuffer) {
            throw new Error\Exception(
                "<pre>\\Tools::printTidyHtml() - Error: " . htmlentities($tidy->errorBuffer . PHP_EOL . PHP_EOL . $tidy->html()->value,
                    ENT_HTML5, 'UTF-8'));
        }
        return print $tidy;
    }
}