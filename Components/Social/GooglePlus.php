<?php
/**
 * Created by PhpStorm.
 * User: chriscunningham
 * Date: 28/12/2013
 * Time: 19:11
 */

namespace FW\Components\Social;

use FW\System\Root;

class GooglePlus extends Google
{
    protected $domain = '';

    public function setDomain($domain)
    {
        $this->domain = $domain;
    }

    public function output()
    {
    }

    public function plusOneButton($domain = '')
    {
        $domain = ' data-href="' . ($domain ?: $this->domain) . '" ';

        return <<<HTML
            <!-- Place this tag where you want the +1 button to render. -->
            <div class="g-plusone"{$domain} data-size="medium" data-annotation="bubble" data-width="120"></div>
HTML;
    }

}