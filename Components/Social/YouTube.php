<?php
/**
 * Created by PhpStorm.
 * User: chriscunningham
 * Date: 30/12/2013
 * Time: 00:43


 */

namespace FW\Components\Social;

use FW\System\Root;

class YouTube extends Google
{
    protected $channelId = '';

    public function setChannelId($channelId)
    {
        $this->channelId = $channelId;
    }

    public function output()
    {
    }

    public function followButton($channelId = '')
    {
        $channelId = $channelId ?: $this->channelId;

        return <<<HTML
            <div class="g-ytsubscribe" data-channelid="{$channelId}" data-layout="default"></div>
HTML;
    }

}