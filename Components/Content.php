<?php
/**
 * @author Chris Cunningham
 */

namespace FW\Components;

use FW\Components\AbstractComponent;

class Content extends AbstractComponent
{
    public function output()
    { ?>
        <div class="content">
        <?php echo $this->getComponentsOutput(); ?>
        </div><?php
    }

}