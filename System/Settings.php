<?php
/**
 * @author Chris Cunningham
 */

use FW\System;
use FW\System\Root;
use FW\System\Utilities\Traits;

/**
 * Global Class
 */
class Settings extends Root\BaseClassStrict
{
    /**
     * @var \Settings
     */
    protected static $instance = null;

    /**
     * @var string
     */
    protected static $iniFilePath = '/App/FW.ini';

    /**
     * @var boolean
     */
    protected $isProduction = false;

    /**
     * @return \Settings
     */
    public static function i()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * @param bool $enableWriteAccess
     */
    protected function __construct($enableWriteAccess = false)
    {
        if (!($settings = $this->getFileSettings())) {
            return $this->setDefaultSettings();
        }
        $this->writeAccess(true);
        $this->data($this->prepareSettings($settings));
        $this->writeAccess($enableWriteAccess);

        if (!$this->isProduction()) {
            $this->setHtmlErrorDocLinks();
        }
    }

    /**
     * @return array
     */
    protected function getFileSettings()
    {
        return \Tools::parseIniFile($this->documentRoot() . self::getIniFilePath());
    }

    /**
     * @param string $iniFilePath
     */
    public static function setIniFilePath($iniFilePath)
    {
        self::$iniFilePath = $iniFilePath;
    }

    /**
     * @return string
     */
    public static function getIniFilePath()
    {
        return self::$iniFilePath;
    }

    /**
     * @param array $settings
     *
     * @return array
     */
    protected function prepareSettings(array $settings)
    {
        $rootSettings = $settings[key($settings)];

        $platform = 'Development';

        if (!isset($settings[$platform])) {
            $this->isProduction = true;
            $platform = 'Production';
        }

        if (isset($rootSettings['useServerSeparatorByOS']) && $rootSettings['useServerSeparatorByOS']) {
            return array_merge($rootSettings, $settings[$platform][trim(PHP_OS)]);
        }
        return array_merge($rootSettings, $settings[$platform]);
    }

    /**
     * @return \Settings
     */
    protected function setDefaultSettings()
    {
        $this->modules(); // Set up with no modules
        return $this;
    }

    /**
     * @return boolean
     */
    public function isProduction()
    {
        return $this->isProduction;
    }

    /**
     * This will enable php to add direct links to php.net documentation about various errors.
     * @return void
     */
    protected function setHtmlErrorDocLinks()
    {
        ini_set('html_errors', 'On');
        ini_set('docref_root', 'http://uk3.php.net/manual/en/');
        ini_set('docref_ext', '.php');
    }

    /**
     * Unified Getters/Setters
     */

    /**
     * @param string $value
     * @param mixed  $defaultData
     *
     * @return string
     */
    public function siteName($value = null, $defaultData = null)
    {
        return $this->access(__FUNCTION__, $value, $defaultData);
    }

    /**
     * @param string $value
     * @param mixed  $defaultData
     *
     * @return string
     */
    public function serverIP($value = null, $defaultData = null)
    {
        return $this->access(__FUNCTION__, $value, $defaultData);
    }


    public function domain($domains = null, $defaultData = null)
    {
        if (is_null($domains)) {
            return $this->access(__FUNCTION__, null, $defaultData);
        }

        if (!isset($_SERVER['SERVER_NAME'])) {
            throw new \Exception($this->getErrorLocation(__FUNCTION__) . " - SERVER_NAME not set in global server array.");
        }

        $currentDomain = $_SERVER['SERVER_NAME'];

        $found = false;
        foreach ($domains as $domain) {
            $quote = preg_quote($domain); // TODO:: @chrisc this is not safe it may handle sub domains but its not good enough, moving on because I need to w finish something else will come back to this.
            if (preg_match("/{$quote}$/", $currentDomain) === 1) {
                $found = true;
                break;
            }
        }

        if (!$found) {
            throw new \Exception($this->getErrorLocation(__FUNCTION__) . " - {$currentDomain} not in domain list.");
        }

        if ($this->canonicalDomain() != $currentDomain) {
            header('HTTP/1.1 301 Moved Permanently');
            header("Location: {$this->canonicalBaseUrl()}");
        }

        return $this->access(__FUNCTION__, $currentDomain, $defaultData);
    }

    /**
     * @return string
     */
    public function scheme()
    {
        return isset($_SERVER['HTTPS']) ? 'https' : 'http';
    }

    /**
     * @return string
     */
    public function baseUrl()
    {
        return "{$this->scheme()}://{$this->domain()}";
    }

    /**
     * @param boolean $value
     * @param mixed   $defaultData
     *
     * @return boolean
     */
    public function canonicalDomain($value = null, $defaultData = null)
    {
        return $this->access(__FUNCTION__, $value, $defaultData);
    }

    /**
     * @return string
     */
    public function canonicalBaseUrl()
    {
        return "{$this->scheme()}://{$this->canonicalDomain()}";
    }

    /**
     * @param boolean $value
     * @param mixed   $defaultData
     *
     * @return boolean
     */
    public function debugSwitch($value = null, $defaultData = null)
    {
        return $this->access(__FUNCTION__, $value, $defaultData);
    }

    /**
     * @param boolean $value
     * @param mixed   $defaultData
     *
     * @return boolean
     */
    public function errorReporting($value = null, $defaultData = null)
    {
        return $this->access(__FUNCTION__, $value, $defaultData);
    }

    /**
     * @param boolean $value
     * @param mixed   $defaultData
     *
     * @return boolean
     */
    public function errorHandlerSwitch($value = null, $defaultData = null)
    {
        return (bool)$this->access(__FUNCTION__, $value, $defaultData);
    }

    /**
     * @param boolean $value
     * @param string  $defaultData
     *
     * @return boolean
     */
    public function displayErrorSwitch($value = null, $defaultData = null)
    {
        return $this->errorHandlerSwitch() && $this->access(__FUNCTION__, $value, $defaultData);
    }

    /**
     * @param boolean $value
     * @param mixed   $defaultData
     *
     * @return boolean
     */
    public function displayExceptionErrorSwitch($value = null, $defaultData = null)
    {
        return $this->errorHandlerSwitch() && $this->access(__FUNCTION__, $value, $defaultData);
    }

    /**
     * @param boolean $value
     * @param string  $defaultData
     *
     * @return boolean
     */
    public function displayErrorLogsSwitch($value = null, $defaultData = null)
    {
        return (bool)$this->errorHandlerSwitch() && $this->access(__FUNCTION__, $value, $defaultData);
    }

    /**
     * @param boolean $value
     * @param string  $defaultData
     *
     * @return boolean
     */
    public function logErrorSwitch($value = null, $defaultData = null)
    {
        return $this->access(__FUNCTION__, $value, $defaultData);
    }

    /**
     * @param boolean $value
     * @param string  $defaultData
     *
     * @return boolean
     */
    public function logExceptionErrorSwitch($value = null, $defaultData = null)
    {
        return $this->errorHandlerSwitch() && $this->access(__FUNCTION__, $value, $defaultData);
    }

    /**
     * @param string  $value
     * @param string  $defaultData
     * @param boolean $addDate
     *
     * @return string
     */
    public function errorLogPath($value = null, $defaultData = null, $addDate = false)
    {
        return $this->access(__FUNCTION__, $value, $defaultData) . ($addDate ? '-' . date('Ymd') : '');
    }

    /**
     * @param boolean $value
     * @param mixed   $defaultData
     *
     * @return string
     */
    public function useServerSeparatorByOS($value = null, $defaultData = null)
    {
        return $this->access(__FUNCTION__, $value, $defaultData);
    }

    /**
     * @param boolean $value
     * @param mixed   $defaultData
     *
     * @return boolean
     */
    public function useTidyHtml($value = null, $defaultData = null)
    {
        return $this->access(__FUNCTION__, $value, $defaultData);
    }

    /**
     * @param boolean $value
     * @param boolean $defaultData
     *
     * @return string
     */
    public function useItemSearchContext($value = null, $defaultData = null)
    {
        return $this->access(__FUNCTION__, $value, $defaultData);
    }

    /**
     * @param string $value
     * @param string $defaultData
     *
     * @return string
     */
    public function googleAnalytics($value = null, $defaultData = null)
    {
        return $this->access(__FUNCTION__, $value, $defaultData);
    }

    /**
     * @param string $value
     * @param string $defaultData
     *
     * @return string
     */
    public function googleVerificationCode($value = null, $defaultData = null)
    {
        return $this->access(__FUNCTION__, $value, $defaultData);
    }

    /**
     * @param string $value
     * @param string $defaultData
     *
     * @return string
     */
    public function faceBookHome($value = null, $defaultData = null)
    {
        return $this->access(__FUNCTION__, $value, $defaultData);
    }

    /**
     * @param string $value
     * @param string $defaultData
     *
     * @return string
     */
    public function twitterUserName($value = null, $defaultData = null)
    {
        return $this->access(__FUNCTION__, $value, $defaultData);
    }


    /**
     * @param string $value
     * @param string $defaultData
     *
     * @return string
     */
    public function youtubeChannelId($value = null, $defaultData = null)
    {
        return $this->access(__FUNCTION__, $value, $defaultData);
    }

    /**
     * @param string $value
     * @param mixed  $defaultData
     *
     * @return string
     */
    public function itemSearchController($value = null, $defaultData = null)
    {
        return $this->access(__FUNCTION__, $value, $defaultData);
    }

    /**
     * @param string $value
     * @param mixed  $defaultData
     *
     * @return string
     */
    public function dateDefaultTimezone($value = null, $defaultData = null)
    {
        if ($value) {
            date_default_timezone_set($value);
        }

        return $this->access(__FUNCTION__, $value, $defaultData);
    }

    /**
     * @param string $value
     * @param mixed  $defaultData
     *
     * @return string
     */
    public function caseSensitiveFileSystem($value = null, $defaultData = true)
    {
        return $this->access(__FUNCTION__, $value, $defaultData);
    }

    /**
     * @param string $value
     * @param mixed  $defaultData
     *
     * @return null
     */
    public function privacyPolicyId($value = null, $defaultData = null)
    {
        return $this->access(__FUNCTION__, $value, $defaultData);
    }

    /**
     * @return bool|string
     */
    public function documentRoot()
    {
        return isset($_SERVER['DOCUMENT_ROOT']) ? rtrim($_SERVER['DOCUMENT_ROOT'], '/') : '';
    }

    /**
     * @param array $data - Array of modules containing their settings
     *
     * @return array
     */
    public function modules(array $data = null)
    {
        if (!$data) {
            return $this->access(__FUNCTION__, null, []);
        }

        foreach ($data as $key => &$moduleSettings) {
            if (!$moduleSettings) {
                unset($data[$key]);
                continue;
            }
            if (is_array($moduleSettings) && array_key_exists(0, $moduleSettings)) {
                $moduleSettings['isEnabled'] = $moduleSettings[0];
                unset($moduleSettings[0]);
            } else {
                if (!$moduleSettings instanceof stdClass) {
                    $moduleSettings = [];
                    $moduleSettings['isEnabled'] = true; // This has to be true here if it is not an array as it has passd the above check for if (!$moduleSettings)
                }
            }
            if (is_array($moduleSettings)) {
                $moduleSettings = fwArrayToStdClass($moduleSettings);
            }
        }

        return $this->access(__FUNCTION__, $data);
    }

}